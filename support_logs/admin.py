import csv
from datetime import datetime

from django.contrib import admin
from django.http import HttpResponse
from django.http.request import codecs

from beneficiaries.models import Beneficiary
from .models import SupportLogs


class StatusFilter(admin.SimpleListFilter):
    title = "Statut"
    parameter_name = "status"

    def lookups(self, _, __):
        return Beneficiary.STATUSES.items()

    def queryset(self, _, queryset):
        if self.value():
            return queryset.filter(status=self.value())

        return queryset


@admin.register(SupportLogs)
class SupportLogsAdmin(admin.ModelAdmin):
    list_display = (
        "get_beneficiary",
        "status",
        "actionDate",
        "get_operator",
        "get_counselor",
        "startSupportDate",
        "endOfSupportDate",
        "get_theme",
        "get_city"
    )
    search_fields = (
        "beneficiary__status",
        "beneficiary__last_name",
        "beneficiary__first_name",
        "operator__email",
        "counselor__email",
        "city__name"
    )
    list_filter = (StatusFilter, "theme")
    ordering = ("-id",)
    actions = ["export_support_logs_to_csv"]

    @admin.display(description="Bénéficiaire")
    def get_beneficiary(self, obj):
        return obj.beneficiary

    @admin.display(description="Opérateur")
    def get_operator(self, obj):
        return obj.operator

    @admin.display(description="Orienteur")
    def get_counselor(self, obj):
        return obj.counselor

    @admin.display(description="Theme")
    def get_theme(self, obj):
        return ", ".join(theme.title for theme in obj.theme.all())

    @admin.display(description="Commune")
    def get_city(self, obj):
        return obj.city

    @admin.action(description="Exporter en csv")
    def export_support_logs_to_csv(self, _, queryset):
        filename = "Export journaux - {}".format(
            datetime.now().strftime("%Y-%m-%d_%H-%M")
        )
        response = HttpResponse(
            content_type="text/csv; charset=utf-8",
            headers={
                "Content-Disposition": 'attachment; filename="{}.csv"'.format(
                    filename
                )
            }
        )
        response.write(codecs.BOM_UTF8)

        writer = csv.writer(response, delimiter=";")
        writer.writerow([
            "Identifiant (base de donnée)",
            "Identifiant bénéficiaire (base de donnée)",
            "Identifiant bénéficiaire unique",
            "Opérateur",
            "Orienteur",
            "Date d'action",
            "Date de début de prise en charge",
            "Date de fin de prise en charge",
            "Thême",
            "Commune"
        ])

        for log in queryset:
            if not log.beneficiary:
                continue

            writer.writerow([
                log.id,
                log.beneficiary.id,
                log.beneficiary.uuid,
                log.operator,
                log.counselor,
                log.actionDate.strftime(
                    "%Y-%m-%d"
                ) if log.actionDate else None,
                log.startSupportDate.strftime(
                    "%Y-%m-%d"
                ) if log.startSupportDate else None,
                log.endOfSupportDate.strftime(
                    "%Y-%m-%d"
                ) if log.endOfSupportDate else None,
                self.get_theme(log),
                log.city
            ])

        return response
