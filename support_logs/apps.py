from django.apps import AppConfig


class SupportLogsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "support_logs"
    verbose_name = "Journaux"
