# Generated by Django 5.0.2 on 2024-03-14 14:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('support_logs', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='supportlogs',
            options={'verbose_name': 'journal', 'verbose_name_plural': 'Journaux'},
        ),
        migrations.AlterField(
            model_name='supportlogs',
            name='endOfSupportDate',
            field=models.DateTimeField(null=True, verbose_name='Date de fin de prise en charge'),
        ),
        migrations.AlterField(
            model_name='supportlogs',
            name='startSupportDate',
            field=models.DateTimeField(null=True, verbose_name='Date de prise en charge'),
        ),
    ]
