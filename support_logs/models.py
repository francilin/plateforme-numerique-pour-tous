import math
from typing import Optional

from django import forms
from django.db import models
from django.forms import ModelForm
from django.utils.safestring import mark_safe

from beneficiaries.models import Beneficiary
from cities.models import City
from support_themes.models import SupportTheme
from users.models import User


class SupportLogs(models.Model):
    VERY_BEGINNER = "VB"
    BEGINNER = "B"
    INTERMEDIATE = "I"
    ADVANCED = "A"
    LEVELS = {
        VERY_BEGINNER: "Totalement accompagné",
        BEGINNER: "Parfois accompagné",
        INTERMEDIATE: "Plutôt autonome",
    }

    beneficiary = models.ForeignKey(
        Beneficiary,
        models.SET_NULL,
        blank=True,
        null=True,
        verbose_name="Bénéficiaire"
    )
    operator = models.ForeignKey(
        User,
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="operator",
        verbose_name="Opérateur",
    )
    counselor = models.ForeignKey(
        User,
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="counselor",
        verbose_name="Orienteur",
    )
    startSupportDate = models.DateTimeField(
        "Date de prise en charge", null=True, blank=True
    )
    endOfSupportDate = models.DateTimeField(
        "Date de fin de prise en charge", null=True, blank=True
    )
    theme = models.ManyToManyField(SupportTheme, blank=True)
    actionDate = models.DateTimeField("Date de l’action")
    city = models.ForeignKey(
        City, models.SET_NULL, blank=True, null=True, verbose_name="Commune"
    )
    status = models.CharField(
        "Statut", choices=Beneficiary.STATUSES, blank=True, null=True
    )
    level = models.CharField(
        "Niveau du bénéficiaire", choices=LEVELS, blank=True, null=True
    )

    def is_creation(self) -> bool:
        return self.status == Beneficiary.CREATED

    def is_support_start(self) -> bool:
        return self.status == Beneficiary.ACTIVE

    def is_support_end(self) -> bool:
        return self.status == Beneficiary.DONE

    def get_city_code(self) -> Optional[str]:
        if self.city is not None:
            return self.city.insee_city_code

        if self.is_creation():
            return None

        # Retrieve city code from start of support
        # end of support dont ask city
        log = SupportLogs.objects.filter(
            operator__structure=self.operator.structure,
            beneficiary=self.beneficiary,
            status=Beneficiary.ACTIVE,
        ).last()

        if log is None or log.city is None:
            return None

        return log.city.insee_city_code

    def get_year(self) -> int:
        return self.actionDate.year

    def get_month(self) -> int:
        return self.actionDate.month

    def get_quarter(self) -> int:
        return math.ceil(self.get_month() / 3)

    class Meta:
        verbose_name = "journal"
        verbose_name_plural = "Journaux"

    def get_level(self):
        return self.LEVELS[self.level]


class StartSupportForm(ModelForm):
    def __init__(self, *args, cities, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["city"].queryset = cities
        self.fields["city"].label = "Commune d'intervention"
        self.fields["city"].required = True
        self.fields["level"].required = True

    class Meta:
        model = SupportLogs
        fields = [
            "startSupportDate",
            "operator",
            "counselor",
            "beneficiary",
            "level",
            "city",
        ]
        widgets = {
            "startSupportDate": forms.DateInput(attrs={"type": "date"}),
            "operator": forms.HiddenInput(),
            "counselor": forms.HiddenInput(),
            "beneficiary": forms.HiddenInput(),
        }


class EndOfSupportForm(ModelForm):
    all_themes = SupportTheme.objects.all()
    theme = forms.ModelMultipleChoiceField(
        queryset=all_themes.order_by("position"),
        help_text=mark_safe(
            "Pour choisir plusieurs thêmes, utilisez <i>ctrl</i> (windows)"
            ", <i>commande</i> (mac os) ou <i>maj</i> enfoncée au clic"
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["theme"].widget.attrs.update(
            {"size": SupportTheme.objects.count(), "class": "overflow-hidden"}
        )

    class Meta:
        model = SupportLogs
        fields = ["endOfSupportDate", "operator", "counselor", "beneficiary", "theme"]
        widgets = {
            "endOfSupportDate": forms.DateInput(attrs={"type": "date"}),
            "operator": forms.HiddenInput(),
            "counselor": forms.HiddenInput(),
            "beneficiary": forms.HiddenInput(),
        }
