from django.db import models


class City(models.Model):
    name = models.CharField("Commune", null=False, blank=False)
    zipcode = models.CharField("Code postal", null=False, blank=False)
    insee_city_code = models.CharField("Code insee", null=False, blank=False)

    def get_entity_verbose_name(self):
        return self._meta.verbose_name

    def __str__(self):
        return f"{self.name} ({self.zipcode})"

    class Meta:
        verbose_name = "Commune"
