from django.contrib import admin

from cities.models import City


@admin.register(City)
class DataAdmin(admin.ModelAdmin):
    def structures(self, obj):
        return ", ".join([
            structure.name for structure in obj.structure_set.all()
        ])

    list_display = (
        "name",
        "zipcode",
        "insee_city_code",
        "structures",
    )
    search_fields = ("name", "zipcode", "insee_city_code")
    ordering = ("zipcode",)
