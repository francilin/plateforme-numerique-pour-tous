[![pipeline status](https://gitlab.com/francilin/plateforme-numerique-pour-tous/badges/main/pipeline.svg)](https://gitlab.com/francilin/plateforme-numerique-pour-tous/-/commits/main)
[![coverage report](https://gitlab.com/francilin/plateforme-numerique-pour-tous/badges/main/coverage.svg)](https://gitlab.com/francilin/plateforme-numerique-pour-tous/-/commits/main)

# Plateforme numérique pour tous

https://numeriquepourtous.metropolegrandparis.fr

## Set configuration

### Install poetry

See https://python-poetry.org/docs/#installation

### Generate secret key

Launch django shell
```sh
poe shell
```

then generate secret
```sh
poe create-secret
```

and put it in `.env` configuration file, so database information too
```sh
# .env
DB_USER=xxxxx
DB_NAME=xxxxx
DB_PASSWORD=x
DB_HOST=xxxxx
DB_PORT=xxxxx
DATABASE_URL=postgres://<DB_USER>:<POSTGRES_PASSWORD>@<DB_HOST>:<DB_PORT>/<DB_NAME>?sslmode=disable
SECRET_KEY=xxxxx #secret key generated above
DEBUG=True
ALLOWED_HOSTS=localhost anotherdomain #for dev
CSRF_TRUSTED_ORIGINS=http://localhost:8000 anotherfullurl #for dev
EMAIL_HOST=…
EMAIL_USE_TLS=…
EMAIL_PORT=…
EMAIL_HOST_USER=…
EMAIL_HOST_PASSWORD='…' # Quote to prevent truncated string
```

## Install dependancies

```sh
poetry install --no-root
```

## Test database connection (optional)

```sh
poe dbshell
```

## Launch migrations

Make sure your user have habilities to create tables
```sql
ALTER USER myuser WITH SUPERUSER;
```

```sql
GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;

```

then migrate
```sh
poe migrate
```

## Load fixtures (optional)

```sh
# Loading only apps fixture
poe fixture [name_of_the_django_app]
```

or

```sh
# Loading fixtures for all apps
poe fixtures
```

## Run development server

```sh
poe runserver
```

## Run tests

```sh
poe test
```

## See all routes

```sh
poe manage show_urls
```

## Docker

### Build docker image

```sh
docker build -t name-the-app .
```

## Deployment (fly.io)

### Install flyctl

https://fly.io/docs/hands-on/install-flyctl/

### Deploy

```sh
poe deploy --env [production|sandbox]
```

### Restore previous version of database

> By default, Clever Cloud performs a free backup every day, with a retention of seven days. Retention and frequency can be customized for Premium customers.
> Each backup can be found in the add-on dashboard in the web console, along with the credentials.
[Source](https://developers.clever-cloud.com/doc/addons/postgresql/#database-daily-backup-and-retention)

### Backup database locally

Download it on clever-cloud addon dashboard, or dump it with psql CLI informations found dashboard too.

### Logs

```sh
flyctl logs -a <app-name>
```

### Fly console

```sh
flyctl ssh console -c fly.[production|sandbox].toml
# On free plan, if no one visiting the website, the app it's sleeping
# So ssh it's fail. Visit website then try again
```

### Create superuser

```sh
flyctl ssh console -c fly.[production|sandbox].toml
#Connecting to … complete
root@x:/code python manage.py createsuperuser
```

### Secrets

#### List them

```sh
flyctl secrets list -a <app-name>
```

#### Set a secret

```sh
flyctl secrets -a <app-name> set SECRET=value
```

#### Show secrets

First connect to the app
```sh
fly ssh console -a <app-name>
```

Then to show `DATABASE_URL`
```sh
echo $DATABASE_URL
```

To show `SECRET_KEY`
```sh
python manage.py shell
```

```python
from plateforme_numerique_pour_tous.settings import SECRET_KEY
print(SECRET_KEY)
```

#### Update secret

`fly secrets -a <app-name> set DATABASE_URL=postgres://example.com/mydb #Quote url if needed`

### Postgres

#### Connect to remote database

You can use PG Studio in clever-cloud add-on

Or connect with psql and connections information found in clever-cloud addon dashboard

### Domain & certificate

https://fly.io/docs/networking/custom-domains-with-fly/

#### Option A & AAAA records

```sh
flyctl ips list -a <app-name>

TYPE ADDRESS        CREATED AT
v4   xx.xx.xxx.xxx  …
v6   xx.xx.xxx.xxx  …
```

> Create an A record pointing to your IPv4 address, and an AAAA record pointing to your IPv6 address. You’re then free to make this an Apex domain as needed.

#### Certificate

```sh
flyctl certs create domain.tld -a <app-name>
```

## Maintenance

### Delete beneficiary | operator | counselor

Deleting beneficiary dont delete its support_log.
So we have to find support_log to clean and delete it in backoffice `/admin/support_logs/supportlogs`

### Delete city | structure

Delete a city hides the associated support_log
