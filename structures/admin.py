import csv
from datetime import datetime

from django.contrib import admin
from django.http import HttpResponse

from .models import Structure


@admin.register(Structure)
class DataAdmin(admin.ModelAdmin):
    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": (
                "cities",
            )
        })
    )

    list_display = (
        "name",
        "related_cities_admin"
    )
    actions = ["export_structures_to_csv"]

    @admin.action(description="Commune(s)")
    def related_cities_admin(self, obj):
        return obj.related_cities()

    @admin.action(description="Exporter en csv")
    def export_structures_to_csv(self, _, queryset):
        filename = "Export structures - {}".format(
            datetime.now().strftime("%Y-%m-%d_%H-%M")
        )
        response = HttpResponse(
            content_type="text/csv",
            headers={
                "Content-Disposition": 'attachment; filename="{}.csv"'.format(
                    filename
                )
            }
        )
        writer = csv.writer(response)
        writer.writerow([
            "Identifiant (base de donnée)",
            "Nom",
            "Commune(s)"
        ])
        for structure in queryset:
            writer.writerow([
                structure.id,
                structure.name,
                structure.related_cities()
            ])

        return response
