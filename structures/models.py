from django.db import models

from cities.models import City


class Structure(models.Model):
    name = models.CharField(
        "Nom",
        max_length=200,
        null=False,
        blank=False
    )
    cities = models.ManyToManyField(
        City,
        verbose_name="Communes"
    )

    def __str__(self):
        return self.name

    def related_cities(self):
        return ", ".join([
            "{} ({})".format(
                city.name, city.zipcode
            ) for city in self.cities.all()
        ])
