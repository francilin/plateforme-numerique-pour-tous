from django.shortcuts import render

def structures_index(request):
    return render(request, "directory/structures_index.html", {"url_name": "structures_index"})
