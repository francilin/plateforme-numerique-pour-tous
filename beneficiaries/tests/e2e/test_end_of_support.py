from datetime import datetime

from django.test import TestCase
from django.urls import reverse

from beneficiaries.models import Beneficiary
from support_logs.models import SupportLogs

from core.tests.utils import (
    create_operator,
    create_counselor,
    create_beneficiary
)
from support_themes.models import SupportTheme


class EndOfSupportBeneficiaryTest(TestCase):
    def test_form_intialized(self):
        user = create_operator()
        beneficiary = create_beneficiary(structure_operator=user.structure)

        self.client.force_login(user)

        response = self.client.get(
            reverse(
                "beneficiaries:end_of_support_form",
                args=[beneficiary.id]
            )
        )

        self.assertContains(response, Beneficiary.DONE)
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(
            response,
            datetime.today().date().strftime("%Y-%m-%d")
        )

    def test_form_button_present_on_listing_for_operator(self):
        user = create_operator()
        create_beneficiary(
            structure_operator=user.structure,
            status=Beneficiary.ACTIVE
        )

        self.client.force_login(user)

        response = self.client.get(
            reverse(
                "beneficiaries:index"
            )
        )

        self.assertContains(
            response,
            "Mettre fin à l'accompagnement"
        )

    def test_counselor_cant_go_to_form(self):
        operator = create_operator()
        beneficiary = create_beneficiary(structure_operator=operator.structure)

        user = create_counselor()
        self.client.force_login(user)

        response = self.client.get(
            reverse(
                "beneficiaries:end_of_support_form",
                args=[beneficiary.id]
            )
        )

        self.assertEqual(response.status_code, 403)

    def test_cant_go_to_form_with_already_done_beneficiary(self):
        user = create_operator()
        beneficiary = create_beneficiary(
            structure_operator=user.structure,
            status=Beneficiary.DONE
        )

        self.client.force_login(user)

        response = self.client.get(
            reverse(
                "beneficiaries:end_of_support_form",
                args=[beneficiary.id]
            )
        )

        self.assertEqual(response.status_code, 403)

    def test_submit_on_already_done_beneficiary_do_not_generate_log(self):
        user = create_operator()
        beneficiary = create_beneficiary(
            structure_operator=user.structure,
            status=Beneficiary.DONE
        )

        self.client.force_login(user)

        response = self.client.post(
            reverse(
                "beneficiaries:end_of_support_action",
                args=[beneficiary.id]
            ),
            data={
                "status": beneficiary.DONE,
                "structure_operator": user.structure.id,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "beneficiary": beneficiary.id,
                "operator": user.id,
                "endOfSupportDate": datetime.today().date().strftime(
                    "%Y-%m-%d"
                ),
                "actionDate": datetime.today().date().strftime("%Y-%m-%d"),
                "theme": [
                    SupportTheme.objects.first().id,
                    SupportTheme.objects.last().id
                ]
            },
            follow=True
        )

        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            len(SupportLogs.objects.filter(beneficiary=beneficiary.id)),
            0
        )

    def test_form_button_not_present_on_listing_for_counselor(self):
        user = create_counselor()
        create_beneficiary(structure_operator=user.structure)

        self.client.force_login(user)

        response = self.client.get(
            reverse(
                "beneficiaries:index"
            )
        )

        self.assertNotContains(
            response,
            "Mettre fin à l'accompagnement"
        )

    def test_end_of_support_send_valid_form(self):
        user = create_operator()
        beneficiary = create_beneficiary(
            structure_operator=user.structure
        )

        self.client.force_login(user)
        response = self.client.post(
            reverse(
                "beneficiaries:end_of_support_action",
                args=[beneficiary.id]
            ),
            data={
                "status": beneficiary.DONE,
                "structure_operator": user.structure.id,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "beneficiary": beneficiary.id,
                "operator": user.id,
                "endOfSupportDate": datetime.today().date().strftime(
                    "%Y-%m-%d"
                ),
                "actionDate": datetime.today().date().strftime("%Y-%m-%d"),
                "theme": [
                    SupportTheme.objects.first().id,
                    SupportTheme.objects.last().id
                ]
            },
            follow=True
        )

        self.assertContains(response, "Terminé")
        self.assertContains(
            response,
            f"Structure de médiation : {user.structure}"
        )
        self.assertEqual(
            SupportLogs.objects.last().theme.first().title,
            SupportTheme.objects.first().title
        )
        self.assertEqual(
            SupportLogs.objects.last().theme.last().title,
            SupportTheme.objects.last().title
        )
        self.assertEqual(SupportLogs.objects.last().status, "done")
