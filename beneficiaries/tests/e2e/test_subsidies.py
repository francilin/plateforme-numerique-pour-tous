import datetime
import math

from django.shortcuts import reverse
from django.test import TestCase

from cities.models import City
from core.tests.utils import create_admin, create_operator, make_start_of_support_log


class SubsidiesTests(TestCase):
    def test_subsidies_differents_operators_but_same_structure(self):
        operator_1 = create_operator(email="operator-1@mail.com")
        operator_2 = create_operator(email="operator-2@mail.com")
        admin = create_admin()
        city = City.objects.get(insee_city_code="75101")

        (
            make_start_of_support_log(
                operator=operator_1,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )
        (
            make_start_of_support_log(
                operator=operator_2,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )

        self.client.force_login(operator_2)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(response.context["subsidies"]["total"], 100)

        self.client.force_login(admin)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(response.context["subsidies"]["total"], 100)

    def test_subsidies_differents_operators_but_different_structure(self):
        operator_1 = create_operator(
            email="operator-1@mail.com", structure_name="Structure A"
        )
        operator_2 = create_operator(
            email="operator-2@mail.com", structure_name="Structure B"
        )
        admin = create_admin()
        city = City.objects.get(insee_city_code="75101")

        (
            make_start_of_support_log(
                operator=operator_1,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )
        (
            make_start_of_support_log(
                operator=operator_2,
                city=city,
                actionDate=datetime.date(2025, 3, 25),
            ).save(),
        )

        self.client.force_login(operator_1)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(response.context["subsidies"]["total"], 50)

        self.client.force_login(operator_2)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(response.context["subsidies"]["total"], 40)

        self.client.force_login(admin)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(response.context["subsidies"]["total"], 90)

    def test_subsidies_reach_max_2024(self):
        operator = create_operator()

        for _ in range(1, 1921):
            make_start_of_support_log(
                operator=operator,
                city=City.objects.get(
                    insee_city_code="7510{}".format(
                        math.ceil(_ / 400)  # Let 400 subsidies by city
                    )
                ),
                actionDate=datetime.date(2024, 3, 27),
            ).save()

        self.client.force_login(operator)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(
            response.context["subsidies"]["alerts"][0],
            "Votre structure a atteint <strong>96</strong> % "
            "des droits à subvention "
            "<strong>pour l’année 2024</strong> (96000 € / max 100000 €)",
        )

    def test_subsidies_reach_max_2025(self):
        operator = create_operator()

        for _ in range(1, 2351):
            make_start_of_support_log(
                operator=operator,
                city=City.objects.get(
                    insee_city_code="7510{}".format(
                        math.ceil(_ / 400)  # Let 400 subsidies by city
                    )
                ),
                actionDate=datetime.date(2025, 3, 27),
            ).save()

        self.client.force_login(operator)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(
            response.context["subsidies"]["alerts"][0],
            "Votre structure a atteint <strong>94</strong> % "
            "des droits à subvention "
            "<strong>pour l’année 2025</strong> (94000 € / max 100000 €)",
        )

    def test_subsidies_reach_max_2026(self):
        operator = create_operator()

        for _ in range(1, 3601):
            make_start_of_support_log(
                operator=operator,
                city=City.objects.get(
                    insee_city_code="7510{}".format(
                        math.ceil(_ / 400)  # Let 400 subsidies by city
                    )
                ),
                actionDate=datetime.date(2026, 3, 27),
            ).save()

        self.client.force_login(operator)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(
            response.context["subsidies"]["alerts"][0],
            "Votre structure a atteint <strong>90</strong> % "
            "des droits à subvention "
            "<strong>pour l’année 2026</strong> (90000 € / max 100000 €)",
        )

    def test_subsidies_reach_max_for_cities(self):
        operator = create_operator()

        for _ in range(1, 721):
            make_start_of_support_log(
                operator=operator,
                city=City.objects.get(
                    insee_city_code="7510{}".format(
                        math.ceil(_ / 360)  # * 50 = 18000 € (90% of the max)
                    )
                ),
                actionDate=datetime.date(2024, 3, 27),
            ).save()

        self.client.force_login(operator)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(
            response.context["subsidies"]["alerts"][0],
            "Votre structure a atteint <strong>90</strong> % "
            "des droits à subvention "
            "sur la commune de <strong>Paris (75001)</strong> "
            "pour l’année 2024 (18000 € / max 20000 €)",
        )
        self.assertEqual(
            response.context["subsidies"]["alerts"][1],
            "Votre structure a atteint <strong>90</strong> % "
            "des droits à subvention "
            "sur la commune de <strong>Paris (75002)</strong> "
            "pour l’année 2024 (18000 € / max 20000 €)",
        )

    def test_subsidies_exceed_max_per_city(self):
        operator = create_operator()

        for _ in range(402):
            make_start_of_support_log(
                operator=operator,
                city=City.objects.get(insee_city_code="75101"),
                actionDate=datetime.date(2024, 3, 27),
            ).save()

        self.client.force_login(operator)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(
            response.context["subsidies"]["alerts"][0],
            "Votre structure a atteint <strong>100</strong> % "
            "des droits à subvention "
            "sur la commune de <strong>Paris (75001)</strong> "
            "pour l’année 2024 (20000 € / max 20000 €)",
        )
