from django.test import TestCase
from django.urls import reverse
from users.models import User

from core.tests.utils import create_beneficiary, create_counselor


class IndexPageTests(TestCase):
    def test_index_empty_page_list_beneficiaries(self):
        user = User.objects.create()
        self.client.force_login(user)
        response = self.client.get(reverse("beneficiaries:index"))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Pas de bénéficiaire")

    def test_index_no_empty_page_list_beneficiaries(self):
        user = User.objects.create()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="F",
            first_name="Louise",
            last_name="Boubarne",
            age="75-",
            phone_number="0606060606"
        )
        create_beneficiary()
        response = self.client.get(reverse("beneficiaries:index"))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, beneficiary.gender)
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(response, beneficiary.last_name)
        self.assertContains(response, beneficiary.phone_number.as_national)

        self.assertContains(response, "Total des bénéficiaires : 2")

    def test_display_only_beneficiaries_attached_to_counselor_structure(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary_with_structure = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=user.structure
        )

        create_beneficiary()  # Beneficiary without structure

        response = self.client.get(reverse("beneficiaries:index"))

        self.assertContains(
            response,
            beneficiary_with_structure.gender
        )
        self.assertContains(
            response,
            beneficiary_with_structure.first_name
        )
        self.assertContains(
            response,
            beneficiary_with_structure.last_name
        )
        self.assertContains(
            response,
            beneficiary_with_structure.phone_number.as_national
        )
        self.assertContains(
            response,
            "Total des bénéficiaires : 1"
        )
