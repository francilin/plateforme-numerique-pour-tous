from django.test import TestCase
from django.urls import reverse
from users.models import User

from core.tests.utils import create_beneficiary


class SearchBeneficiaryByPhoneNumberTests(TestCase):
    def setUp(self):
        user = User.objects.create()
        self.client.force_login(user)

    def test_search_empty_form(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"phone_number": "", "search_by_uuid": ""}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "au moins 1 des 2")

    def test_search_without_beneficiary(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"phone_number": "0600000000"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Pas de bénéficiaire")

    def test_search_unexsiting_beneficiary(self):
        create_beneficiary(
            phone_number="0600000000",
        )

        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"phone_number": "0600000001"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Pas de bénéficiaire")

    def test_find_exact_phone_number_match(self):
        beneficiary = create_beneficiary(phone_number="0600000002")

        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"phone_number": "0600000002"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, beneficiary.last_name)

    def test_fail_wrong_caracters(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"phone_number": "aenuisetanuisetanuiesta"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            "Saisissez un numéro de téléphone valide"
        )

    def test_fail_to_few_digit(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"phone_number": "06"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            "Saisissez un numéro de téléphone valide"
        )
