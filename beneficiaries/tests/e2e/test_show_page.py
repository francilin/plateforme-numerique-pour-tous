from django.test import TestCase
from django.urls import reverse

from core.tests.utils import (
    create_beneficiary,
    create_counselor,
    create_operator
)
from structures.models import Structure


class ShowPageTests(TestCase):
    def test_show_beneficiary_for_counselor(self):
        user = create_counselor()
        beneficiary = create_beneficiary(structure_counselor=user.structure)
        self.client.force_login(user)

        response = self.client.get(reverse(
            "beneficiaries:show",
            args=[beneficiary.id]
        ))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, beneficiary.gender)
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(response, beneficiary.last_name)
        self.assertContains(response, beneficiary.phone_number.as_national)
        self.assertContains(response, beneficiary.get_huuid())
        self.assertContains(response, "Éditer")

    def test_show_beneficiary_for_operator(self):
        user = create_operator()
        beneficiary = create_beneficiary(structure_operator=user.structure)
        self.client.force_login(user)

        response = self.client.get(reverse(
            "beneficiaries:show",
            args=[beneficiary.id]
        ))

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, beneficiary.gender)
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(response, beneficiary.last_name)
        self.assertContains(response, beneficiary.phone_number.as_national)
        self.assertContains(response, beneficiary.get_huuid())
        self.assertContains(response, "Éditer le numéro de téléphone")

    def test_show_beneficiary_for_counselor_but_different_structure(self):
        user = create_counselor()
        structure = Structure(name="another structure")
        structure.save()
        beneficiary = create_beneficiary(structure_counselor=structure)
        self.client.force_login(user)

        response = self.client.get(reverse(
            "beneficiaries:show",
            args=[beneficiary.id]
        ))

        self.assertEqual(response.status_code, 403)

    def test_show_beneficiary_for_operator_but_different_structure(self):
        user = create_operator()
        structure = Structure(name="another structure")
        structure.save()
        beneficiary = create_beneficiary(structure_operator=structure)
        self.client.force_login(user)

        response = self.client.get(reverse(
            "beneficiaries:show",
            args=[beneficiary.id]
        ))

        self.assertEqual(response.status_code, 403)
