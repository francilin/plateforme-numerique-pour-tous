from django.test import TestCase
from django.urls import reverse
from users.models import User

from core.tests.utils import create_beneficiary


class SearchBeneficiaryByUUIDTests(TestCase):
    def setUp(self):
        user = User.objects.create()
        self.client.force_login(user)

    def test_search_without_beneficiary(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": "uuuuuuu"}
        )

        self.assertContains(response, "Pas de bénéficiaire")

    def test_search_unexisting_beneficiary(self):
        beneficiary = create_beneficiary()
        uuid = beneficiary.uuid
        # reversed to by different and shorted
        bad_uuid = str(uuid)[::-1][:7]

        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": bad_uuid}
        )

        self.assertContains(response, "Pas de bénéficiaire")

    def test_find_by_uuid_7_characters(self):
        beneficiary = create_beneficiary()
        pattern = str(beneficiary.uuid)[:7]

        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": pattern}
        )

        self.assertNotContains(response, "Pas de bénéficiaire")
        self.assertContains(response, beneficiary.last_name)

    def test_find_by_uuid_8_characters(self):
        beneficiary = create_beneficiary()
        pattern = "{}-{}".format(
            str(beneficiary.uuid)[:4],
            str(beneficiary.uuid)[4:7]
        )

        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": pattern}
        )

        self.assertNotContains(response, "Pas de bénéficiaire")
        self.assertContains(response, beneficiary.last_name)

    def test_find_by_uuid_find_different_case(self):
        beneficiary = create_beneficiary()
        pattern = "{}-{}".format(
            str(beneficiary.uuid)[:4],
            str(beneficiary.uuid)[4:7]
        )

        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": pattern.upper()}
        )

        self.assertNotContains(response, "Pas de bénéficiaire")
        self.assertContains(response, beneficiary.last_name)

    def test_fail_to_few_character(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": "uu"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            "au moins 7"
        )

    def test_fail_too_much_characters(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            data={"search_by_uuid": "123456789"}
        )

        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            "au plus 8"
        )
