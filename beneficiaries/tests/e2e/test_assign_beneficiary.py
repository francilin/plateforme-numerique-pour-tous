from datetime import datetime

from django.test import TestCase
from django.urls import reverse

from beneficiaries.models import Beneficiary
from cities.models import City
from support_logs.models import SupportLogs
from users.models import User

from core.tests.utils import (
    create_beneficiary,
    create_counselor,
    create_operator
)


class AssignBeneficiaryTest(TestCase):
    def test_display_assign_beneficiary_form(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary()
        response = self.client.get(
            reverse(
                "beneficiaries:assign_form",
                args=[beneficiary.id]
            )
        )

        self.assertContains(
            response,
            f"Profil bénéficiaire de : {beneficiary}"
        )
        self.assertContains(
            response,
            "Prendre en charge"
        )

    def test_success_assign_display_beneficiary_after_save(self):
        city = City(name="Commune", zipcode="00000")
        city.save()

        user = create_operator()
        user.structure.cities.add(city)
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={
                "status": beneficiary.ACTIVE,
                "structure_operator": user.structure.id,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "beneficiary": beneficiary.id,
                "operator": user.id,
                "startSupportDate": datetime.today().date().strftime(
                    "%Y-%m-%d"
                ),
                "actionDate": datetime.today().date().strftime("%Y-%m-%d"),
                "city": city.id,
                "level": SupportLogs.BEGINNER
            },
            follow=True
        )

        self.assertContains(response, beneficiary.gender)
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(response, beneficiary.last_name)
        self.assertContains(response, user.structure)
        self.assertContains(response, "Pris en charge")
        self.assertContains(response, SupportLogs.BEGINNER)
        self.assertEqual(SupportLogs.objects.last().status, "active")

    def test_fail_submit_empty_form(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={},
            follow=True
        )

        self.assertContains(response, "Ce champ est obligatoire")

    def test_fail_assign_for_user_without_group(self):
        user = User.objects.create()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={"status": "active"},
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_fail_assign_for_user_in_counselor_group(self):
        user = create_counselor()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={"status": "active"},
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_fail_assign_form_with_beneficiary_with_active_status(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary(status=Beneficiary.ACTIVE)

        response = self.client.get(
            reverse(
                "beneficiaries:assign_form",
                args=[beneficiary.id]
            )
        )

        self.assertEqual(response.status_code, 403)

    def test_fail_assign_submit_form_with_beneficiary_with_active_status(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary(status=Beneficiary.ACTIVE)

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={"status": "active"},
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_fail_assign_without_city(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={
                "status": beneficiary.ACTIVE,
                "structure_operator": user.structure.id,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "beneficiary": beneficiary.id,
                "operator": user.id,
                "startSupportDate": datetime.today().date().strftime(
                    "%Y-%m-%d"
                ),
                "actionDate": datetime.today().date().strftime("%Y-%m-%d"),
            },
            follow=True
        )

        self.assertContains(response, "Ce champ est obligatoire")

    def test_fail_assign_without_level(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={
                "status": beneficiary.ACTIVE,
                "structure_operator": user.structure.id,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "beneficiary": beneficiary.id,
                "operator": user.id,
                "startSupportDate": datetime.today().date().strftime(
                    "%Y-%m-%d"
                ),
                "actionDate": datetime.today().date().strftime("%Y-%m-%d"),
                "city": City.objects.last().id
            },
            follow=True
        )

        self.assertContains(response, "Ce champ est obligatoire")
