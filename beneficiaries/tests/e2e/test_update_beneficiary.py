from django.test import TestCase
from django.urls import reverse
from core.tests.utils import (
    create_beneficiary,
    create_counselor,
    create_operator
)


class UpdateBeneficiaryTest(TestCase):
    def setUp(self):
        self.user = create_counselor()
        self.client.force_login(self.user)

    def test_edit_form_load_beneficiary(self):
        beneficiary = create_beneficiary(gender="M")

        response = self.client.get(
            reverse("beneficiaries:edit", args=[beneficiary.id])
        )

        self.assertContains(response, "Monsieur")
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(response, beneficiary.last_name)
        self.assertContains(response, beneficiary.age)
        self.assertContains(response, beneficiary.phone_number.as_national)

    def test_success_update_display_beneficiary_after_save(self):
        old_beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        new_beneficiary = {
            "gender": "W",
            "first_name": "Tata",
            "last_name": "Alamontagne",
            "age": "35-39",
            "phone_number": "0617171717",
            "structure_counselor": self.user.structure.id
        }

        response = self.client.post(
            reverse("beneficiaries:change", args=[old_beneficiary.id]),
            data=new_beneficiary,
            follow=True
        )

        self.assertContains(response, new_beneficiary["gender"])
        self.assertContains(response, new_beneficiary["first_name"])
        self.assertContains(response, new_beneficiary["last_name"])
        self.assertContains(response, "06 17 17 17 17")
        self.assertContains(response, "Total des bénéficiaires : 1")

    def test_success_update_beneficiary_with_no_change_value(self):
        beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[beneficiary.id]),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": beneficiary.phone_number,
                "structure_counselor": beneficiary.structure_counselor
            },
            follow=True
        )

        self.assertNotContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_success_update_phone_number_with_dot(self):
        old_beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        new_beneficiary = {
            "gender": "W",
            "first_name": "Tata",
            "last_name": "Alamontagne",
            "age": "35-39",
            "phone_number": "06.17.17.17.17",
            "structure_counselor": self.user.structure.id
        }

        response = self.client.post(
            reverse("beneficiaries:change", args=[old_beneficiary.id]),
            data=new_beneficiary,
            follow=True
        )

        self.assertContains(response, "06 17 17 17 17")
        self.assertContains(response, "Total des bénéficiaires : 1")

    def test_success_update_phone_number_with_prefix(self):
        old_beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        new_beneficiary = {
            "gender": "W",
            "first_name": "Tata",
            "last_name": "Alamontagne",
            "age": "35-39",
            "phone_number": "+33617171717",
            "structure_counselor": self.user.structure.id
        }

        response = self.client.post(
            reverse("beneficiaries:change", args=[old_beneficiary.id]),
            data=new_beneficiary,
            follow=True
        )

        self.assertContains(response, "06 17 17 17 17")
        self.assertContains(response, "Total des bénéficiaires : 1")

    def test_success_update_phone_number_with_space(self):
        old_beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        new_beneficiary = {
            "gender": "W",
            "first_name": "Tata",
            "last_name": "Alamontagne",
            "age": "35-39",
            "phone_number": "06 17 17 17 17",
            "structure_counselor": self.user.structure.id
        }

        response = self.client.post(
            reverse("beneficiaries:change", args=[old_beneficiary.id]),
            data=new_beneficiary,
            follow=True
        )

        self.assertContains(response, "06 17 17 17 17")
        self.assertContains(response, "Total des bénéficiaires : 1")

    def test_success_update_phone_number_with_07(self):
        old_beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        new_beneficiary = {
            "gender": "W",
            "first_name": "Tata",
            "last_name": "Alamontagne",
            "age": "35-39",
            "phone_number": "07 47 17 17 17",
            "structure_counselor": self.user.structure.id
        }

        response = self.client.post(
            reverse("beneficiaries:change", args=[old_beneficiary.id]),
            data=new_beneficiary,
            follow=True
        )

        self.assertContains(response, "07 47 17 17 17")
        self.assertContains(response, "Total des bénéficiaires : 1")

    def test_success_update_same_beneficiary_but_phone_number(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616",
            structure_counselor=self.user.structure
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[first_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": second_beneficiary.first_name,
                "last_name": second_beneficiary.last_name,
                "age": "20-24",
                "phone_number": first_beneficiary.phone_number,
                "structure_counselor": self.user.structure.id
            },
            follow=True
        )

        self.assertContains(response, second_beneficiary.first_name)
        self.assertContains(response, second_beneficiary.last_name)
        self.assertContains(response, "06 16 16 16 16")
        self.assertNotContains(response, "06 26 16 16 16")
        self.assertContains(response, "Total des bénéficiaires : 2")

    def test_success_update_beneficiary_comment(self):
        beneficiary = create_beneficiary(
            gender="M",
            first_name="Toto",
            last_name="Alaplage",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=self.user.structure,
            comment="old comment"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[beneficiary.id]),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": beneficiary.phone_number,
                "structure_counselor": beneficiary.structure_counselor,
                "comment": "New comment"
            },
            follow=True
        )

        self.assertNotContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )
        self.assertNotContains(response, beneficiary.comment)
        self.assertContains(response, "New comment")

    def test_fail_to_update_unexisting_beneficiary(self):
        beneficiary = {
            "gender": "W",
            "first_name": "Tata",
            "last_name": "Alamontagne",
            "age": "35-39",
            "phone_number": "06 17 17 17 17"
        }

        response = self.client.post(
            reverse("beneficiaries:change", args=[1]),
            data=beneficiary,
            follow=True
        )

        self.assertEqual(response.status_code, 404)

    def test_fail_to_update_same_beneficiary(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[first_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": second_beneficiary.first_name,
                "last_name": second_beneficiary.last_name,
                "age": "20-24",
                "phone_number": second_beneficiary.phone_number.as_national,
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_first_name_different_case(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[first_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": "marie",
                "last_name": second_beneficiary.last_name,
                "age": "20-24",
                "phone_number": second_beneficiary.phone_number.as_national,
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_last_name_different_case(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[first_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": second_beneficiary.first_name,
                "last_name": "curie",
                "age": "20-24",
                "phone_number": second_beneficiary.phone_number.as_national,
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_with_accent(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="Francois",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[second_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": "François",
                "last_name": first_beneficiary.last_name,
                "age": "20-24",
                "phone_number": first_beneficiary.phone_number.as_national,
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_without_accent(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[second_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": "Francois",
                "last_name": first_beneficiary.last_name,
                "age": "20-24",
                "phone_number": first_beneficiary.phone_number.as_national,
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_phone_number_with_dot(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[second_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": first_beneficiary.first_name,
                "last_name": first_beneficiary.last_name,
                "age": "20-24",
                "phone_number": "06.16.16.16.16",
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_phone_number_with_prefix(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[second_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": first_beneficiary.first_name,
                "last_name": first_beneficiary.last_name,
                "age": "20-24",
                "phone_number": "+33616161616",
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_update_same_beneficiary_but_phone_number_with_space(self):
        first_beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        second_beneficiary = create_beneficiary(
            gender="W",
            first_name="Marie",
            last_name="Curie",
            age="25-29",
            phone_number="0626161616"
        )

        response = self.client.post(
            reverse("beneficiaries:change", args=[second_beneficiary.id]),
            data={
                "gender": "M",
                "first_name": first_beneficiary.first_name,
                "last_name": first_beneficiary.last_name,
                "age": "20-24",
                "phone_number": "06 16 16 16 16",
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_edit_form_not_available_for_operator(self):
        self.user = create_operator(email="operator@mail.com")
        self.client.force_login(self.user)

        beneficiary = create_beneficiary()

        response = self.client.get(
            reverse("beneficiaries:edit", args=[beneficiary.id])
        )

        self.assertEqual(response.status_code, 403)

    def test_edit_not_available_for_operator(self):
        beneficiary = create_beneficiary()

        self.user = create_operator(email="operator@mail.com")
        self.client.force_login(self.user)

        response = self.client.post(
            reverse("beneficiaries:change", args=[beneficiary.id]),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.gender,
                "phone_number": beneficiary.phone_number,
            },
            follow=True
        )

        self.assertEqual(response.status_code, 403)
