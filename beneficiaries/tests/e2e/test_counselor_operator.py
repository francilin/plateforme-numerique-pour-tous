from datetime import datetime

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import Group
from cities.models import City
from support_logs.models import SupportLogs

from users.models import User, GROUP_COUNSELOR_NAME, GROUP_OPERATOR_NAME
from structures.models import Structure
from core.tests.utils import create_beneficiary


class CounselorOperatorTest(TestCase):
    def test_groups_are_displayed(self):
        user = User.objects.create()

        group_operator = Group.objects.get(name=GROUP_OPERATOR_NAME)
        group_counselor = Group.objects.get(name=GROUP_COUNSELOR_NAME)
        user.groups.add(group_operator, group_counselor)

        structure = Structure(name="Structure de test")
        structure.save()

        user.structure = structure
        user.save()
        self.client.force_login(user)

        response = self.client.get(
            reverse("beneficiaries:index")
        )

        self.assertContains(response, GROUP_COUNSELOR_NAME)

    def test_beneficiaries_create_and_assign_are_displayed(self):
        user = User.objects.create()
        city = City(name="Commune", zipcode="00000")
        city.save()

        group_operator = Group.objects.get(name=GROUP_OPERATOR_NAME)
        group_counselor = Group.objects.get(name=GROUP_COUNSELOR_NAME)
        user.groups.add(group_operator, group_counselor)

        structure = Structure(name="Structure de test")
        structure.save()

        user.structure = structure
        user.structure.cities.add(city)
        user.save()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        beneficiary_2 = {
            "gender": "W",
            "first_name": "Jacques",
            "last_name": "Dupond",
            "age": "40-44",
            "phone_number": "0616457469",
            "structure_counselor": user.structure.id,
            "comment": ""
        }

        self.client.post(
            reverse("beneficiaries:create"),
            data=beneficiary_2,
            follow=True
        )

        response = self.client.post(
            reverse("beneficiaries:assign_action", args=[beneficiary.id]),
            data={
                "status": beneficiary.ACTIVE,
                "structure_operator": user.structure.id,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "beneficiary": beneficiary.id,
                "operator": user.id,
                "startSupportDate": datetime.today().date().strftime(
                    "%Y-%m-%d"
                ),
                "actionDate": datetime.today().date().strftime("%Y-%m-%d"),
                "city": city.id,
                "level": SupportLogs.VERY_BEGINNER
            },
            follow=True
        )

        self.assertContains(response, beneficiary.gender)
        self.assertContains(response, beneficiary.first_name)
        self.assertContains(response, beneficiary.last_name)
        self.assertContains(response, user.structure)
        self.assertContains(response, "Pris en charge")
        self.assertContains(response, SupportLogs.VERY_BEGINNER)

        response = self.client.get(reverse("beneficiaries:index"))

        self.assertContains(response, "Total des bénéficiaires : 2")
