from django.test import TestCase
from django.urls import reverse

from beneficiaries.models import Beneficiary
from support_logs.models import SupportLogs
from users.models import User

from core.tests.utils import (
    create_beneficiary,
    create_operator,
    create_counselor
)


class CreateBeneficiaryTest(TestCase):
    def test_display_create_beneficiary_form(self):
        user = create_counselor()
        self.client.force_login(user)
        response = self.client.get(reverse("beneficiaries:new"))

        self.assertContains(response, "Nouveau bénéficiaire")

    def test_success_create_display_beneficiary_after_save(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = {
            "gender": "W",
            "first_name": "Jacques",
            "last_name": "Dupond",
            "age": "40-44",
            "phone_number": "0616457469",
            "structure_counselor": user.structure.id,
            "comment": ""
        }

        response = self.client.post(
            reverse("beneficiaries:create"),
            data=beneficiary,
            follow=True
        )

        self.assertContains(response, beneficiary["gender"])
        self.assertContains(response, beneficiary["first_name"])
        self.assertContains(response, beneficiary["last_name"])
        self.assertContains(response, "06 16 45 74 69")
        self.assertContains(response, "Total des bénéficiaires : 1")
        self.assertContains(
            response,
            "Son code d'identification est : <strong>{}</strong>".format(
                Beneficiary.objects.last().get_huuid()
            )
        )
        self.assertEqual(SupportLogs.objects.last().status, "created")

    def test_success_create_phone_number_with_dot(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = {
            "gender": "W",
            "first_name": "Jacques",
            "last_name": "Dupond",
            "age": "40-44",
            "phone_number": "06.16.45.74.69",
            "structure_counselor": user.structure.id,
            "comment": ""
        }

        response = self.client.post(
            reverse("beneficiaries:create"),
            data=beneficiary,
            follow=True
        )

        self.assertContains(response, "06 16 45 74 69")
        self.assertContains(response, "Total des bénéficiaires : 1")
        self.assertContains(
            response,
            "Son code d'identification est : <strong>{}</strong>".format(
                Beneficiary.objects.last().get_huuid()
            )
        )
        self.assertEqual(SupportLogs.objects.last().status, "created")

    def test_success_create_phone_number_with_prefix(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = {
            "gender": "W",
            "first_name": "Jacques",
            "last_name": "Dupond",
            "age": "40-44",
            "phone_number": "+33616457469",
            "structure_counselor": user.structure.id,
            "comment": ""
        }

        response = self.client.post(
            reverse("beneficiaries:create"),
            data=beneficiary,
            follow=True
        )

        self.assertContains(response, "06 16 45 74 69")
        self.assertContains(response, "Total des bénéficiaires : 1")
        self.assertContains(
            response,
            "Son code d'identification est : <strong>{}</strong>".format(
                Beneficiary.objects.last().get_huuid()
            )
        )
        self.assertEqual(SupportLogs.objects.last().status, "created")

    def test_success_create_phone_number_with_space(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = {
            "gender": "W",
            "first_name": "Jacques",
            "last_name": "Dupond",
            "age": "40-44",
            "phone_number": "06 16 45 74 69",
            "structure_counselor": user.structure.id,
            "comment": ""
        }

        response = self.client.post(
            reverse("beneficiaries:create"),
            data=beneficiary,
            follow=True
        )

        self.assertContains(response, "06 16 45 74 69")
        self.assertContains(response, "Total des bénéficiaires : 1")
        self.assertContains(
            response,
            "Son code d'identification est : <strong>{}</strong>".format(
                Beneficiary.objects.last().get_huuid()
            )
        )
        self.assertEqual(SupportLogs.objects.last().status, "created")

    def test_success_create_phone_number_with_07(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = {
            "gender": "W",
            "first_name": "Jacques",
            "last_name": "Dupond",
            "age": "40-44",
            "phone_number": "0740000000",
            "structure_counselor": user.structure.id,
            "comment": ""
        }

        response = self.client.post(
            reverse("beneficiaries:create"),
            data=beneficiary,
            follow=True
        )

        self.assertContains(response, "07 40 00 00 00")
        self.assertContains(response, "Total des bénéficiaires : 1")
        self.assertContains(
            response,
            "Son code d'identification est : <strong>{}</strong>".format(
                Beneficiary.objects.last().get_huuid()
            )
        )
        self.assertEqual(SupportLogs.objects.last().status, "created")

    def test_success_create_same_beneficiary_but_phone_number(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616",
            structure_counselor=user.structure
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": "0616161617",
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(response, "06 16 16 16 17")
        self.assertContains(response, "Total des bénéficiaires : 2")
        self.assertContains(
            response,
            "Son code d'identification est : <strong>{}</strong>".format(
                Beneficiary.objects.last().get_huuid()
            )
        )
        self.assertEqual(SupportLogs.objects.last().status, "created")

    def test_fail_create_for_user_without_group(self):
        user = User.objects.create()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": "0616161617",
                "structure_counselor": "",
                "comment": ""
            },
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_fail_create_for_user_in_operator_group(self):
        user = create_operator()
        self.client.force_login(user)

        beneficiary = create_beneficiary()

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": "0616161617",
                "structure_counselor": "",
                "comment": ""
            },
            follow=True
        )

        self.assertEqual(response.status_code, 403)

    def test_fail_to_create_beneficiary_with_empty_form(self):
        user = create_counselor()
        self.client.force_login(user)
        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": "M",
                "first_name": " ",
                "last_name": " ",
                "age": " ",
                "phone_number": " ",
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "Ce champ est obligatoire"
        )
        self.assertContains(
            response,
            "Saisissez un numéro de téléphone valide"
        )

    def test_fail_to_create_same_beneficiary(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="Albert",
            last_name="Einstein",
            age="20-24",
            phone_number="0616161616"
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": beneficiary.phone_number.as_national,
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_create_same_beneficiary_but_with_accent(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="Francois",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": "François",
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": beneficiary.phone_number.as_national,
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_create_same_beneficiary_but_without_accent(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": "Francois",
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": beneficiary.phone_number.as_national,
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_create_same_beneficiary_but_phone_number_with_dot(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": "06.16.16.16.16",
                "phone_number": beneficiary.phone_number.as_national,
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_create_same_beneficiary_but_phone_number_with_prefix(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616",
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": "+33616161616",
                "phone_number": beneficiary.phone_number.as_national,
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_fail_create_same_beneficiary_but_phone_number_with_space(self):
        user = create_counselor()
        self.client.force_login(user)
        beneficiary = create_beneficiary(
            gender="M",
            first_name="François",
            last_name="Gillet",
            age="20-24",
            phone_number="0616161616"
        )

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": beneficiary.gender,
                "first_name": beneficiary.first_name,
                "last_name": beneficiary.last_name,
                "age": beneficiary.age,
                "phone_number": "06 16 16 16 16",
                "structure_counselor": user.structure.id,
                "comment": ""
            },
            follow=True
        )

        self.assertContains(
            response,
            "déjà existant avec le même nom et numéro de téléphone"
        )

    def test_create_form_not_available_for_operator(self):
        self.user = create_operator(email="operator@mail.com")
        self.client.force_login(self.user)

        response = self.client.get(
            reverse("beneficiaries:new")
        )

        self.assertEqual(response.status_code, 403)

    def test_create_not_available_for_operator(self):
        self.user = create_operator(email="operator@mail.com")
        self.client.force_login(self.user)

        response = self.client.post(
            reverse("beneficiaries:create"),
            data={
                "gender": "W",
                "first_name": "First",
                "last_name": "L",
                "age": "20-24",
                "phone_number": "06 16 16 16 16",
                "comment": ""
            },
            follow=True
        )

        self.assertEqual(response.status_code, 403)
