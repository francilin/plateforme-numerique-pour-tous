import datetime

from django.shortcuts import reverse
from django.test import TestCase

from cities.models import City
from core.tests.utils import (
    create_city_user,
    create_operator,
    make_start_of_support_log,
)


class SubsidiesForCityGroupTests(TestCase):
    def test_two_different_city(self):
        city_user = create_city_user()  # Created with last city
        same_city = city_user.city
        another_city = City.objects.first()
        operator = create_operator()

        (
            make_start_of_support_log(
                operator=operator,
                city=same_city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )

        (
            make_start_of_support_log(
                operator=operator,
                city=another_city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )

        self.client.force_login(city_user)
        response = self.client.get(reverse("subsidies_index"))
        self.assertEqual(response.context["subsidies"]["total"], 50)

    def test_same_city_different_operator(self):
        city_user = create_city_user()  # Created with last city
        city = city_user.city
        operator1 = create_operator()
        operator2 = create_operator(email="operator2@mail.coop")

        (
            make_start_of_support_log(
                operator=operator1,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )

        (
            make_start_of_support_log(
                operator=operator2,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save(),
        )

        self.client.force_login(city_user)
        response = self.client.get(reverse("subsidies_index"))
        self.assertEqual(response.context["subsidies"]["total"], 100)

    def test_max_exceeded(self):
        city_user = create_city_user()  # Created with last city
        city = city_user.city
        operator = create_operator()

        for _ in range(0, 410):
            make_start_of_support_log(
                operator=operator,
                city=City.objects.get(insee_city_code=city.insee_city_code),
                actionDate=datetime.date(2024, 3, 27),
            ).save()

        self.client.force_login(city_user)
        response = self.client.get(reverse("subsidies_index"))

        self.assertEqual(response.context["subsidies"]["total"], 20_000)
        per_year = response.context["subsidies"]["per_year"]
        self.assertEqual(per_year["2024"]["total"], 20_000)
        self.assertEqual(per_year["2024"]["startSupportCount"], 410)
        self.assertEqual(per_year["2024"]["t1"]["subsidies"], 20_000)
        self.assertEqual(per_year["2024"]["t1"]["startSupportCount"], 410)

        per_city = response.context["subsidies"]["per_city"][
            city_user.city.insee_city_code
        ]
        self.assertEqual(per_city["per_year"]["2024"]["total"], 20_000)
        self.assertEqual(per_city["per_year"]["2024"]["startSupportCount"], 410)
