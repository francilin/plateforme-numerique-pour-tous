from django.test import TestCase

from ... models import Beneficiary


class BeneficiaryModelTests(TestCase):
    def test_the_meta_value_of_the_model(self):
        self.assertEqual(Beneficiary._meta.verbose_name, 'bénéficiaire')

    def test_the_string_value_of_the_model(self):
        r = Beneficiary(last_name="Amar", first_name="Tarik")
        self.assertEqual(r.__str__(), "Tarik Amar")

    def test_new_beneficiary_has_uuid(self):
        b = Beneficiary(last_name="One", first_name="1", gender="m")
        b.save()
        self.assertIsNotNone(b.uuid)

        b2 = Beneficiary(last_name="Two", first_name="2", gender="m")
        b2.save()

        self.assertNotEqual(b.uuid, b2.uuid)

    def test_beneficiary_human_readable_uuid(self):
        b = Beneficiary(last_name="One", first_name="1", gender="m")
        b.save()

        self.assertTrue(b.get_huuid().replace("-", "") in str(b.uuid))
