import datetime
import math
from typing import List, TypedDict

from django.test import TestCase

from beneficiaries.views import _compute_subsidies
from cities.models import City
from core.tests.utils import (
    create_beneficiary,
    create_operator,
    create_operator_counselor,
    make_end_of_support_log,
    make_start_of_support_log,
)
from support_logs.models import SupportLogs


class SubsidiesTests(TestCase):
    def test_subsidies_for_2024_admissions(self):
        operator = create_operator()
        operator_counselor = create_operator_counselor()
        beneficiary = create_beneficiary()
        city = City.objects.get(insee_city_code="75101")

        class TestCase(TypedDict):
            support_logs: List[SupportLogs]
            total: int
            total_t1: int
            total_t2: int

        def _test_cases_operator(operator) -> List[TestCase]:
            # Save one log with startSupportDate
            # to handle endOfSupport (missing city)
            make_start_of_support_log(
                beneficiary=beneficiary,
                operator=operator,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save()
            return [
                {
                    "support_logs": [
                        make_start_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            city=city,
                            actionDate=datetime.date(2024, 3, 25),
                        ),
                        make_end_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            actionDate=datetime.date(2024, 4, 25),
                        ),
                    ],
                    "total": 100,
                    "total_t1": 50,
                    "total_t2": 50,
                },
                {
                    "support_logs": [
                        make_start_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            city=city,
                            actionDate=datetime.date(2024, 3, 26),
                        ),
                        make_end_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            actionDate=datetime.date(2024, 4, 26),
                        ),
                        make_start_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            city=city,
                            actionDate=datetime.date(2024, 3, 27),
                        ),
                        make_end_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            actionDate=datetime.date(2024, 4, 27),
                        ),
                    ],
                    "total": 200,
                    "total_t1": 100,
                    "total_t2": 100,
                },
                {
                    "support_logs": [],
                    "total": 0,
                    "total_t1": 0,
                    "total_t2": 0,
                },
            ]

        other_operator_same_struct = create_operator(email="operator2@ma.il")

        test_case_mixed_users: TestCase = {
            "support_logs": [
                make_start_of_support_log(
                    beneficiary=beneficiary,
                    operator=operator,
                    city=city,
                    actionDate=datetime.date(2024, 3, 26),
                ),
                make_end_of_support_log(
                    beneficiary=beneficiary,
                    operator=other_operator_same_struct,
                    actionDate=datetime.date(2024, 3, 27),
                ),
            ],
            "total": 100,
            "total_t1": 100,
            "total_t2": 0,
        }

        for tc in (
            _test_cases_operator(operator)
            + _test_cases_operator(operator_counselor)
            + [test_case_mixed_users]
        ):
            subsidies = _compute_subsidies(tc["support_logs"])

            self.assertEqual(subsidies["total"], tc["total"])
            self.assertEqual(
                subsidies["per_year"]["2024"]["t1"]["subsidies"], tc["total_t1"]
            )
            self.assertEqual(
                subsidies["per_year"]["2024"]["t2"]["subsidies"], tc["total_t2"]
            )

    def test_subsidies_for_2025_admissions(self):
        operator = create_operator()
        operator_counselor = create_operator_counselor()
        beneficiary = create_beneficiary()
        city = City.objects.get(insee_city_code="75101")

        def _test_cases_operator(operator):
            # Save one log with startSupportDate
            # to handle endOfSupport (missing city)
            make_start_of_support_log(
                beneficiary=beneficiary,
                operator=operator,
                city=city,
                actionDate=datetime.date(2024, 3, 25),
            ).save()
            return [
                {
                    "support_logs": [
                        make_start_of_support_log(
                            operator=operator,
                            actionDate=datetime.date(2025, 6, 25),
                            city=city,
                        )
                    ],
                    "total": 40,
                    "total_t1": 0,
                    "total_t2": 40,
                },
                {
                    "support_logs": [
                        make_start_of_support_log(
                            operator=operator,
                            city=city,
                            actionDate=datetime.date(2025, 3, 26),
                        ),
                        make_end_of_support_log(
                            beneficiary=beneficiary,
                            operator=operator,
                            actionDate=datetime.date(2025, 6, 27),
                        ),
                    ],
                    "total": 80,
                    "total_t1": 40,
                    "total_t2": 40,
                },
                {
                    "support_logs": [],
                    "total": 0,
                    "total_t1": 0,
                    "total_t2": 0,
                },
            ]

        for tc in _test_cases_operator(operator) + _test_cases_operator(
            operator_counselor
        ):
            subsidies = _compute_subsidies(tc["support_logs"])
            self.assertEqual(subsidies["total"], tc["total"])
            self.assertEqual(
                subsidies["per_year"]["2025"]["t1"]["subsidies"], tc["total_t1"]
            )
            self.assertEqual(
                subsidies["per_year"]["2025"]["t2"]["subsidies"], tc["total_t2"]
            )

    def test_subsidies_for_2026_admissions(self):
        operator = create_operator()
        city = City.objects.get(insee_city_code="75101")

        test_cases = [
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2026, 1, 25),
                    )
                ],
                "total": 25,
                "total_t1": 25,
                "total_t2": 0,
                "total_t3": 0,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2026, 4, 26),
                    ),
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2026, 7, 27),
                    ),
                ],
                "total": 50,
                "total_t1": 0,
                "total_t2": 25,
                "total_t3": 25,
            },
            {
                "support_logs": [],
                "total": 0,
                "total_t1": 0,
                "total_t2": 0,
                "total_t3": 0,
            },
        ]

        for tc in test_cases:
            subsidies = _compute_subsidies(tc["support_logs"])
            self.assertEqual(subsidies["total"], tc["total"])
            self.assertEqual(
                subsidies["per_year"]["2026"]["t1"]["subsidies"], tc["total_t1"]
            )
            self.assertEqual(
                subsidies["per_year"]["2026"]["t2"]["subsidies"], tc["total_t2"]
            )
            self.assertEqual(
                subsidies["per_year"]["2026"]["t3"]["subsidies"], tc["total_t3"]
            )

    def test_subsidies_for_unhandled_years(self):
        operator = create_operator()
        city = City.objects.get(insee_city_code="75101")

        test_cases = [
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2023, 3, 25),
                    )
                ],
                "total_subsidies": 0,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2027, 3, 26),
                    ),
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2027, 3, 27),
                    ),
                ],
                "total_subsidies": 0,
            },
        ]

        for tc in test_cases:
            subsidies = _compute_subsidies(tc["support_logs"])
            self.assertEqual(subsidies["total"], tc["total_subsidies"])

    def test_subsidies_mixed_handled_unhandled_years(self):
        operator = create_operator()
        city = City.objects.get(insee_city_code="75101")

        test_cases = [
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2023, 3, 25),
                    )
                ],
                "total_subsidies": 0,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2024, 3, 25),
                    )
                ],
                "total_subsidies": 50,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2024, 3, 26),
                    ),
                    make_start_of_support_log(
                        operator=operator,
                        city=city,
                        actionDate=datetime.date(2024, 3, 27),
                    ),
                ],
                "total_subsidies": 100,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        actionDate=datetime.date(2025, 3, 25),
                        operator=operator,
                        city=city,
                    )
                ],
                "total_subsidies": 40,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        actionDate=datetime.date(2025, 3, 26),
                        operator=operator,
                        city=city,
                    ),
                    make_start_of_support_log(
                        actionDate=datetime.date(2025, 3, 27),
                        operator=operator,
                        city=city,
                    ),
                ],
                "total_subsidies": 80,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        actionDate=datetime.date(2026, 3, 25),
                        operator=operator,
                        city=city,
                    )
                ],
                "total_subsidies": 25,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        actionDate=datetime.date(2026, 3, 26),
                        operator=operator,
                        city=city,
                    ),
                    make_start_of_support_log(
                        actionDate=datetime.date(2026, 3, 27),
                        operator=operator,
                        city=city,
                    ),
                ],
                "total_subsidies": 50,
            },
            {
                "support_logs": [
                    make_start_of_support_log(
                        actionDate=datetime.date(2027, 3, 26),
                        operator=operator,
                        city=city,
                    ),
                    make_start_of_support_log(
                        actionDate=datetime.date(2027, 3, 27),
                        operator=operator,
                        city=city,
                    ),
                ],
                "total_subsidies": 0,
            },
        ]

        for tc in test_cases:
            subsidies = _compute_subsidies(tc["support_logs"])
            self.assertEqual(subsidies["total"], tc["total_subsidies"])

    def test_max_subsidies_for_a_structure_2024(self):
        operator = create_operator()

        subsidies = _compute_subsidies(
            [
                make_start_of_support_log(
                    actionDate=datetime.date(2024, 3, 27),
                    operator=operator,
                    city=City.objects.get(
                        insee_city_code="7510{}".format(
                            math.ceil(_ / 400)  # Let 400 subsidies by city
                        )
                    ),
                )
                for _ in range(1, 2002)
            ]
        )

        self.assertEqual(subsidies["total"], 100_000, "total all years failed")
        self.assertEqual(
            subsidies["per_year"]["2024"]["total"], 100_000, "total 2024 failed"
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t1"]["subsidies"],
            100_000,
            "total 2024 T1 failed",
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t2"]["subsidies"], 0, "total 2024 T2 failed"
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t3"]["subsidies"], 0, "total 2024 T3 failed"
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t4"]["subsidies"], 0, "total 2024 T4 failed"
        )

    def test_max_subsidies_for_a_structure_during_whole_2024(self):
        operator = create_operator()

        subsidies = _compute_subsidies(
            [
                make_start_of_support_log(
                    actionDate=datetime.date(
                        2024,
                        math.ceil(index / 167),
                        27,  # 2004 / 12 = 167
                    ),
                    operator=operator,
                    city=City.objects.get(
                        insee_city_code="7510{}".format(
                            math.ceil(index / 400)  # Let 400 subsidies by city
                        )
                    ),
                )
                for index in range(1, 2004)  # 2004 * 50 = 100200 €
            ]
        )

        self.assertEqual(subsidies["total"], 100_000, "total all years failed")
        self.assertEqual(
            subsidies["per_year"]["2024"]["total"], 100_000, "total 2024 failed"
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t1"]["subsidies"],
            25050,
            "total 2024 T1 failed",
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t2"]["subsidies"],
            25050,
            "total 2024 T2 failed",
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t3"]["subsidies"],
            25050,
            "total 2024 T3 failed",
        )
        self.assertEqual(
            subsidies["per_year"]["2024"]["t4"]["subsidies"],
            24850,
            "total 2024 T4 failed",
        )

    def test_max_subsidies_for_a_city(self):
        operator = create_operator()

        subsidies = _compute_subsidies(
            [
                make_start_of_support_log(
                    actionDate=datetime.date(2024, 1, 27),
                    operator=operator,
                    city=City.objects.get(insee_city_code="75101"),
                )
                for _ in range(0, 401)  # 401 * 50 = 20050 €
            ]
        )

        self.assertEqual(subsidies["total"], 20_000, "total all years failed")
        self.assertEqual(
            subsidies["per_city"]["75101"]["per_year"]["2024"]["total"],
            20_000,
            "total 2024 failed",
        )
