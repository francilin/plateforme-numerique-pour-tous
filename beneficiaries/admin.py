import csv
from datetime import datetime

from django.contrib import admin
from django.http import HttpResponse
from django.http.request import codecs

from .models import Beneficiary


@admin.register(Beneficiary)
class BeneficiaryAdmin(admin.ModelAdmin):
    list_display = (
        "gender",
        "last_name",
        "first_name",
        "status",
        "phone_number",
        "structure_counselor",
        "structure_operator"
    )
    search_fields = (
        "phone_number",
        "first_name",
        "last_name",
        "structure_operator__name",
        "structure_counselor__name"
    )
    ordering = ("-id",)
    actions = ["export_beneficiaries_to_csv"]

    @admin.action(description="Exporter en csv")
    def export_beneficiaries_to_csv(self, _, queryset):
        filename = "Export bénéficiaires - {}".format(
            datetime.now().strftime("%Y-%m-%d_%H-%M")
        )
        response = HttpResponse(
            content_type="text/csv; charset=utf-8",
            headers={
                "Content-Disposition": 'attachment; filename="{}.csv"'.format(
                    filename
                )
            }
        )
        response.write(codecs.BOM_UTF8)

        writer = csv.writer(response, delimiter=";")
        writer.writerow([
            "Identifiant (base de donnée)",
            "Identifiant unique",
            "Civilité",
            "Age",
            "Numéro de téléphone",
            "Statut",
            "Structure d’orientation",
            "Structure de médiation",
        ])

        for beneficiary in queryset:
            writer.writerow([
                beneficiary.id,
                beneficiary.uuid,
                beneficiary.get_gender(),
                beneficiary.age,
                beneficiary.phone_number.as_national,
                beneficiary.get_status(),
                beneficiary.structure_counselor,
                beneficiary.structure_operator,
            ])

        return response
