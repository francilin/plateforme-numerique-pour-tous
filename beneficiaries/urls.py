from django.urls import path

from . import views

app_name = "beneficiaries"

urlpatterns = [
    path("", views.index, name="index"),

    path("<int:beneficiary_id>/", views.show, name="show"),

    path("<int:beneficiary_id>/edition", views.edit, name="edit"),
    path("<int:beneficiary_id>/change", views.change, name="change"),
    path(
        "<int:beneficiary_id>/edition-telephone",
        views.edit_phone,
        name="edit_phone"
    ),
    path(
        "<int:beneficiary_id>/change-phone",
        views.change_phone,
        name="change_phone"
    ),

    path("nouveau", views.new, name="new"),
    path("create", views.create, name="create"),

    path(
        "<int:beneficiary_id>/prise-en-charge",
        views.assign_form,
        name="assign_form"
    ),

    path(
        "<int:beneficiary_id>/assign",
        views.assign_action,
        name="assign_action"
    ),

    path(
        "<int:beneficiary_id>/fin-accompagnement",
        views.end_of_support_form,
        name="end_of_support_form"
    ),
    path(
        "<int:beneficiary_id>/end-of-support",
        views.end_of_support_action,
        name="end_of_support_action"
    ),
]
