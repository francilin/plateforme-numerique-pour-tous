import copy
from datetime import datetime
from typing import List

from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.core.exceptions import BadRequest, PermissionDenied
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumber

from cities.models import City
from structures.models import Structure
from support_logs.models import EndOfSupportForm, StartSupportForm, SupportLogs
from support_themes.models import SupportTheme
from users.models import GROUP_CITY_NAME, GROUP_COUNSELOR_NAME, GROUP_OPERATOR_NAME

from .models import (
    Beneficiary,
    BeneficiaryForm,
    BeneficiaryFormAssign,
    BeneficiaryFormEndOfSupport,
    BeneficiaryFormPhone,
    BeneficiaryFormSearch,
    Subsidy,
)

MAX_SUB_PER_YEAR = 100_000
MAX_SUB_PER_CITY = 20_000


def index(request):
    user = request.user

    if user.groups.filter(name=GROUP_CITY_NAME).exists():
        return HttpResponseRedirect(reverse("subsidies_index"))

    if (
        user.groups.filter(name=GROUP_OPERATOR_NAME).exists()
        and user.groups.filter(name=GROUP_COUNSELOR_NAME).exists()
    ):
        beneficiaries = Beneficiary.objects.filter(
            Q(structure_operator=user.structure.id)
            | Q(structure_counselor=user.structure.id)
        )
        title = """Les bénéficiaires orientés ou
        pris en charge par votre structure"""
    elif user.groups.filter(name=GROUP_COUNSELOR_NAME).exists():
        beneficiaries = Beneficiary.objects.filter(
            structure_counselor=user.structure.id
        )
        title = "Les bénéficiaires orientés par votre structure"
    elif user.groups.filter(name=GROUP_OPERATOR_NAME).exists():
        beneficiaries = Beneficiary.objects.filter(structure_operator=user.structure.id)
        title = "Les bénéficiaires pris en charge par votre structure"
    else:
        beneficiaries = Beneficiary.objects.all()
        title = "Tous les bénéficiaires"

    if request.GET:
        return _search(request, beneficiaries.order_by("-id"), title)

    initial = {"uuid": None}
    form = BeneficiaryFormSearch(initial=initial)

    return render(
        request,
        "beneficiaries/index.html",
        {
            "beneficiary_list": beneficiaries.order_by("status", "last_name"),
            "form": form,
            "title": title,
        },
    )


def _search(request, assigned_beneficiaries, title):
    form = BeneficiaryFormSearch(request.GET)
    structure = request.user.structure
    phone_number = request.GET.get("phone_number", "")
    uuid = request.GET.get("search_by_uuid", "")

    if form.is_valid():
        if phone_number:
            phone_number_normalized = PhoneNumber.from_string(phone_number, region="FR")
            beneficiaries = Beneficiary.objects.filter(
                phone_number=phone_number_normalized
            )

        if uuid:
            beneficiaries = Beneficiary.objects.filter(
                uuid__istartswith=uuid.replace("-", "")
            )
    else:
        beneficiaries = Beneficiary.objects.all()

    return render(
        request,
        "beneficiaries/index.html",
        {
            "beneficiary_list": assigned_beneficiaries,
            "beneficiary_list_assigned": beneficiaries.filter(
                structure_operator=structure
            ),
            "beneficiary_list_active": beneficiaries.filter(
                status=Beneficiary.ACTIVE
            ).exclude(structure_operator=structure),
            "beneficiary_list_other": beneficiaries.exclude(
                structure_operator=structure
            ).exclude(status=Beneficiary.ACTIVE),
            "form": form,
            "title": title,
            "type": "search" if form.is_valid() else None,
            "beneficiaries_count": beneficiaries.count() if form.is_valid() else None,
        },
    )


@permission_required("beneficiaries.view_beneficiary", raise_exception=True)
def show(request, beneficiary_id):
    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    user = request.user
    if _get_structure_according_user(beneficiary, user) != user.structure:
        raise PermissionDenied()

    beneficiary_log = SupportLogs.objects.filter(beneficiary_id=beneficiary_id)

    beneficiary_level = None
    if beneficiary_log is not None:
        level_log = beneficiary_log.filter(level__isnull=False)

        if level_log:
            beneficiary_level = level_log.last().get_level()

    created_at = None
    if beneficiary_log is not None:
        created_log = beneficiary_log.filter(status=Beneficiary.CREATED).first()

        if created_log:
            created_at = created_log.actionDate

    return render(
        request,
        "beneficiaries/show.html",
        {
            "beneficiary": beneficiary,
            "beneficiary_level": beneficiary_level,
            "created_at": created_at,
        },
    )


@permission_required("beneficiaries.change_beneficiary", raise_exception=True)
def edit(request, beneficiary_id):
    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    form = BeneficiaryForm(instance=beneficiary)

    return render(
        request, "beneficiaries/edit.html", {"beneficiary": beneficiary, "form": form}
    )


@permission_required("beneficiaries.change_beneficiary", raise_exception=True)
def change(request, beneficiary_id):
    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    form = BeneficiaryForm(request.POST, instance=beneficiary)
    beneficiary.gender = request.POST["gender"]
    beneficiary.first_name = request.POST["first_name"]
    beneficiary.last_name = request.POST["last_name"]
    beneficiary.age = request.POST["age"]
    beneficiary.phone_number = request.POST["phone_number"]

    if form.is_valid():
        beneficiary.save()
        return HttpResponseRedirect(reverse("beneficiaries:index"))

    return render(
        request, "beneficiaries/edit.html", {"beneficiary": beneficiary, "form": form}
    )


@permission_required("beneficiaries.change_beneficiary_phone", raise_exception=True)
def edit_phone(request, beneficiary_id):
    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    form = BeneficiaryFormPhone(instance=beneficiary)

    return render(
        request,
        "beneficiaries/edit_phone.html",
        {"beneficiary": beneficiary, "form": form},
    )


@permission_required("beneficiaries.change_beneficiary_phone", raise_exception=True)
def change_phone(request, beneficiary_id):
    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    form = BeneficiaryFormPhone(request.POST, instance=beneficiary)

    if form.is_valid():
        beneficiary.save()
        return HttpResponseRedirect(reverse("beneficiaries:index"))

    return render(
        request,
        "beneficiaries/edit_phone.html",
        {"beneficiary": beneficiary, "form": form},
    )


@permission_required("beneficiaries.add_beneficiary", raise_exception=True)
def new(request):
    user = request.user
    initial = {}

    if user.groups.filter(name=GROUP_COUNSELOR_NAME).exists():
        initial.update({"structure_counselor": user.structure.id})

    form = BeneficiaryForm(initial=initial)

    return render(request, "beneficiaries/new.html", {"form": form})


@permission_required("beneficiaries.add_beneficiary", raise_exception=True)
def create(request):
    form = BeneficiaryForm(request.POST)

    structure = (
        get_object_or_404(Structure, pk=request.POST["structure_counselor"])
        if request.POST["structure_counselor"]
        else None
    )

    beneficiary = Beneficiary(
        gender=request.POST["gender"],
        first_name=request.POST["first_name"],
        last_name=request.POST["last_name"],
        age=request.POST["age"],
        phone_number=request.POST["phone_number"],
        structure_counselor=structure,
        comment=request.POST["comment"],
    )

    if form.is_valid():
        user = request.user
        beneficiary.save()
        SupportLogs(
            beneficiary=beneficiary,
            operator=user if user.is_operator() else None,
            counselor=user if user.is_counselor() else None,
            actionDate=datetime.today(),
            status=Beneficiary.CREATED,
        ).save()
        messages.add_message(
            request,
            messages.SUCCESS,
            "<h5>Bénéficiaire créé avec succès ! \
            Son code d'identification est : <strong>{}</strong></h5> \
            <p>La prise en charge du bénéficiaire peut être faite via \
            son numéro de téléphone ou via ce code. \
            Il peut être retrouvé \
            dans la fiche détail du bénéficiaire.</p>".format(beneficiary.get_huuid()),
        )
        return HttpResponseRedirect(reverse("beneficiaries:index"))

    return render(
        request, "beneficiaries/new.html", {"beneficiary": beneficiary, "form": form}
    )


@permission_required("beneficiaries.assign_beneficiary", raise_exception=True)
def assign_form(request, beneficiary_id):
    user = request.user

    if user.is_staff:
        raise PermissionDenied()

    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    if beneficiary.status != Beneficiary.CREATED:
        raise PermissionDenied()

    initial = {
        "status": Beneficiary.ACTIVE,
        "startSupportDate": datetime.today().date().strftime("%Y-%m-%d"),
        "beneficiary": beneficiary,
        "operator": user if user.is_operator() else None,
        "counselor": user if user.is_counselor() else None,
    }

    if user.groups.filter(name=GROUP_OPERATOR_NAME).exists():
        initial.update({"structure_operator": user.structure.id})

    startSupportLogsform = StartSupportForm(
        initial=initial, cities=user.structure.cities.all()
    )

    form = BeneficiaryFormAssign(instance=beneficiary, initial=initial)

    return render(
        request,
        "beneficiaries/assign_form.html",
        {
            "beneficiary": beneficiary,
            "form": form,
            "start_support_form": startSupportLogsform,
        },
    )


@permission_required("beneficiaries.assign_beneficiary", raise_exception=True)
def assign_action(request, beneficiary_id):
    user = request.user

    if user.is_staff:
        raise PermissionDenied()

    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    if beneficiary.status != Beneficiary.CREATED:
        raise PermissionDenied()

    startSupportLogsform = StartSupportForm(
        request.POST, cities=user.structure.cities.all()
    )
    form = BeneficiaryFormAssign(request.POST, instance=beneficiary)

    if form.is_valid() and startSupportLogsform.is_valid():
        beneficiary.status = Beneficiary.ACTIVE
        beneficiary.save()

        city = get_object_or_404(City, pk=request.POST["city"])
        supportLog = SupportLogs(
            beneficiary=beneficiary,
            operator=user if user.is_operator() else None,
            counselor=user if user.is_counselor() else None,
            startSupportDate=request.POST["startSupportDate"],
            city=city,
            actionDate=datetime.today(),
            level=request.POST["level"],
            status=Beneficiary.ACTIVE,
        )
        supportLog.save()

        return HttpResponseRedirect(
            reverse("beneficiaries:show", args=[beneficiary_id])
        )

    return render(
        request,
        "beneficiaries/assign_form.html",
        {
            "beneficiary": beneficiary,
            "form": form,
            "start_support_form": startSupportLogsform,
        },
    )


@permission_required("beneficiaries.end_of_support_beneficiary", raise_exception=True)
def end_of_support_form(request, beneficiary_id):
    user = request.user

    if user.is_staff:
        raise PermissionDenied()

    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    if beneficiary.status == Beneficiary.DONE:
        raise PermissionDenied()

    initial = {
        "status": Beneficiary.DONE,
        "endOfSupportDate": datetime.today().date().strftime("%Y-%m-%d"),
        "beneficiary": beneficiary,
        "operator": user if user.is_operator() else None,
        "counselor": user if user.is_counselor() else None,
    }

    endOfSupportLogsform = EndOfSupportForm(initial=initial)
    form = BeneficiaryFormEndOfSupport(initial=initial, instance=beneficiary)

    return render(
        request,
        "beneficiaries/end_of_support_form.html",
        {
            "beneficiary": beneficiary,
            "form": form,
            "end_of_support_form": endOfSupportLogsform,
        },
    )


@permission_required("beneficiaries.end_of_support_beneficiary", raise_exception=True)
def end_of_support_action(request, beneficiary_id):
    user = request.user
    beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_id)

    if user.is_staff or beneficiary.status == Beneficiary.DONE:
        raise PermissionDenied()

    endOfSupportLogsform = EndOfSupportForm(request.POST)
    form = BeneficiaryFormEndOfSupport(request.POST)
    beneficiary.status = Beneficiary.DONE
    themes = SupportTheme.objects.filter(id__in=request.POST.getlist("theme"))

    if form.is_valid() and endOfSupportLogsform.is_valid():
        beneficiary.save()
        supportLog = SupportLogs(
            beneficiary=beneficiary,
            operator=user if user.is_operator() else None,
            counselor=user if user.is_counselor() else None,
            endOfSupportDate=request.POST["endOfSupportDate"],
            actionDate=datetime.today(),
            status=Beneficiary.DONE,
        )
        supportLog.save()
        supportLog.theme.set(themes)

        return HttpResponseRedirect(
            reverse("beneficiaries:show", args=[beneficiary_id])
        )

    return render(
        request,
        "beneficiaries/end_of_support_form.html",
        {
            "beneficiary": beneficiary,
            "form": form,
            "end_of_support_form": endOfSupportLogsform,
        },
    )


@permission_required("beneficiaries.view_subsidies", raise_exception=True)
def subsidies_index(request):
    user = request.user

    if user.is_staff:
        supportLogs = SupportLogs.objects.all()
    elif user.is_operator():
        supportLogs = SupportLogs.objects.filter(operator__structure=user.structure)
    elif user.is_city_group():
        supportLogs = SupportLogs.objects.filter(city=user.city)
    else:
        raise BadRequest()

    subsidies = _compute_subsidies(supportLogs.order_by("id"), user.is_staff)

    return render(
        request,
        "beneficiaries/subsidies_index.html",
        {
            "subsidies": subsidies,
            "total_start_support": len(supportLogs.filter(status=Beneficiary.ACTIVE)),
            "total_end_support": len(supportLogs.filter(status=Beneficiary.DONE)),
        },
    )


def _compute_subsidies(supportLogs: List[SupportLogs], is_staff: bool = False):
    (
        subsidies_by_year,
        subsidies_by_city,
        subsidies_by_structure,
        total,
    ) = _format_subsidies_data(supportLogs, is_staff).values()

    alerts = []

    alerts += _build_alerts_for_subsidies_by_year(subsidies_by_year)
    alerts += _build_alerts_for_subsidies_by_city(subsidies_by_city)

    return {
        "total": total,
        "per_year": subsidies_by_year,
        "per_city": subsidies_by_city,
        "per_structure": subsidies_by_structure,
        "alerts": alerts,
    }


def _format_subsidies_data(supportLogs: List[SupportLogs], is_staff: bool):
    subsidies_by_city = {}
    subsidies_by_year = copy.deepcopy(Subsidy.subsidies_by_year)
    subsidies_by_structure = {}

    for supportLog in supportLogs:
        city_code = supportLog.get_city_code()

        if city_code and city_code not in subsidies_by_city:
            subsidies_by_city[city_code] = copy.deepcopy(Subsidy.subsidies_by_city)
            subsidies_by_city[city_code]["fullname"] = supportLog.city

        structure = None

        if supportLog.operator:
            structure = supportLog.operator.structure

        # Counselor is ignored
        if structure is None:
            continue

        if structure not in subsidies_by_structure:
            subsidies_by_structure[structure] = copy.deepcopy(
                Subsidy.subsidies_by_structure
            )

        subsidy = _subsidy_for(supportLog)
        year = str(supportLog.get_year())
        quarter = "t" + str(supportLog.get_quarter())

        # Count support
        try:
            if supportLog.is_creation():
                subsidies_by_year[year]["new"] += 1
                subsidies_by_year[year][quarter]["new"] += 1

            elif supportLog.is_support_start():
                subsidies_by_year[year][quarter]["startSupportCount"] += 1
                subsidies_by_year[year]["startSupportCount"] += 1
                subsidies_by_city[city_code]["per_year"][year]["startSupportCount"] += 1
                subsidies_by_structure[structure]["per_year"][year][
                    "startSupportCount"
                ] += 1

            elif supportLog.is_support_end():
                subsidies_by_year[year][quarter]["endSupportCount"] += 1
                subsidies_by_year[year]["endSupportCount"] += 1
                subsidies_by_city[city_code]["per_year"][year]["endSupportCount"] += 1
                subsidies_by_structure[structure]["per_year"][year][
                    "endSupportCount"
                ] += 1

        except KeyError:
            continue

        # Sum or cap subs
        try:
            if (
                subsidies_by_city[city_code]["per_year"][year]["total"] + subsidy
                <= MAX_SUB_PER_CITY
            ):
                if structure is None:
                    continue

                if (
                    subsidies_by_structure[structure]["per_year"][year]["total"]
                    >= MAX_SUB_PER_YEAR
                ):
                    subsidies_by_year[year]["total"] = MAX_SUB_PER_YEAR
                    subsidies_by_structure[structure]["per_year"][year][
                        "total"
                    ] = MAX_SUB_PER_YEAR
                else:
                    subsidies_by_year[year]["total"] += subsidy
                    subsidies_by_year[year][quarter]["subsidies"] += subsidy
                    subsidies_by_structure[structure]["per_year"][year][
                        "total"
                    ] += subsidy
                    subsidies_by_city[city_code]["per_year"][year]["total"] += subsidy
            else:
                subsidies_by_city[city_code]["per_year"][year][
                    "total"
                ] = MAX_SUB_PER_CITY

        except KeyError:
            continue

    return {
        "subsidies_by_year": subsidies_by_year,
        "subsidies_by_city": subsidies_by_city,
        "subsidies_by_structure": subsidies_by_structure if is_staff else None,
        "total": _compute_total(subsidies_by_structure),
    }


def _compute_total(subsidies_by_structure):
    total = 0

    for structure in subsidies_by_structure.values():
        for year in structure["per_year"].values():
            total += year["total"]

    return total


def _opens_right_to_subsidy(supportLog: SupportLogs) -> bool:
    return supportLog.is_support_start() or supportLog.is_support_end()


def _subsidy_for(supportLog: SupportLogs):
    if not _opens_right_to_subsidy(supportLog):
        return 0

    year = supportLog.get_year()

    if year == 2024:
        return 100 / 2

    if year == 2025:
        return 80 / 2

    if year == 2026:
        return 50 / 2

    return 0


def _build_alerts_for_subsidies_by_year(subsidies_by_year):
    alerts = []

    for year, subsidies in subsidies_by_year.items():
        percent = subsidies["total"] / MAX_SUB_PER_YEAR

        if percent < 0.9:
            continue

        alerts.append(
            "Votre structure a atteint <strong>{percent}</strong> % "
            "des droits à subvention"
            " <strong>pour l’année {year}</strong> "
            "({total} € / max {max} €)".format(
                percent=round(percent * 100),
                year=year,
                total=round(subsidies["total"]),
                max=MAX_SUB_PER_YEAR,
            ),
        )

    return alerts


def _build_alerts_for_subsidies_by_city(subsidies_by_city):
    alerts = []

    for insee_city_code, data in subsidies_by_city.items():
        for year_by_city, data_by_city in data["per_year"].items():
            percent = data_by_city["total"] / MAX_SUB_PER_CITY

            if percent < 0.9:
                continue

            alerts.append(
                "Votre structure a atteint <strong>{percent}</strong> % "
                "des droits à subvention"
                " sur la commune de <strong>{city}</strong>"
                " pour l’année {year} ({total} € / max {max} €)".format(
                    percent=round(percent * 100),
                    city=City.objects.get(insee_city_code=insee_city_code),
                    year=year_by_city,
                    total=round(data_by_city["total"]),
                    max=MAX_SUB_PER_CITY,
                ),
            )

    return alerts


def _get_structure_according_user(beneficiary, user):
    if user.is_operator():
        return beneficiary.structure_operator

    if user.is_counselor():
        return beneficiary.structure_counselor
