# Generated by Django 5.0.2 on 2024-02-21 10:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('beneficiaries', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='beneficiary',
            name='gender',
            field=models.CharField(choices=[('W', 'Femme'), ('M', 'Homme')], default=None, max_length=1, verbose_name='Femme / Homme'),
        ),
    ]
