# Generated by Django 5.0.2 on 2024-03-07 10:10

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("beneficiaries", "0006_beneficiary_structure_counselor"),
        ("structures", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="beneficiary",
            name="structure_operator",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="structure_operator",
                to="structures.structure",
            ),
        ),
        migrations.AlterField(
            model_name="beneficiary",
            name="structure_counselor",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="structure_counselor",
                to="structures.structure",
            ),
        ),
    ]
