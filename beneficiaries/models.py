import uuid

from django import forms
from django.core import validators
from django.db import models
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.forms import ModelForm

from phonenumber_field.modelfields import PhoneNumberField

from structures.models import Structure


class Beneficiary(models.Model):
    MAN = "M"
    WOMAN = "W"
    GENDERS = {WOMAN: "Madame", MAN: "Monsieur"}
    AGE_GROUP = {
        "": "Choisissez votre classe d’âge ici",
        "16-19": "16-19",
        "20-24": "20-24",
        "25-29": "25-29",
        "30-34": "30-34",
        "35-39": "35-39",
        "40-44": "40-44",
        "45-49": "45-49",
        "50-54": "50-54",
        "55-59": "55-59",
        "60-64": "60-64",
        "65-69": "65-69",
        "70-74": "70-74",
        "75-": "75 ou +",
    }
    CREATED = "created"
    ACTIVE = "active"
    DONE = "done"
    EXPIRED = "expired"
    STATUSES = {
        CREATED: "Actif",
        ACTIVE: "Pris en charge",
        DONE: "Terminé",
        EXPIRED: "Expiré",
    }

    gender = models.CharField(
        "Madame / Monsieur",
        max_length=1,
        choices=GENDERS,
        null=False,
        blank=False,
        default=None,  # To avoid another radio for unselected
    )
    last_name = models.CharField("Nom", max_length=200, null=False, blank=False)
    first_name = models.CharField("Prénom", max_length=200, null=False, blank=False)
    age = models.CharField(
        "Classe d’âge", max_length=5, choices=AGE_GROUP, null=False, blank=False
    )
    phone_number = PhoneNumberField(
        "Numéro de téléphone",
        region="FR",
        null=False,
        blank=False,
        help_text="Formats acceptés : \
            01.02.03.04.05, 0102030405, +33102030405, 01 02 03 04 05",
    )
    status = models.CharField("Statut", choices=STATUSES, default=CREATED)
    structure_counselor = models.ForeignKey(
        Structure,
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="structure_counselor",
        verbose_name="Structure d’orientation",
    )
    structure_operator = models.ForeignKey(
        Structure,
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="structure_operator",
        verbose_name="Structure de prise en charge",
    )
    # unique is setted in migration due to some technical constraint
    uuid = models.UUIDField(
        "Code d’identification unique",
        primary_key=False,
        default=uuid.uuid4,
        help_text="Formats acceptés : 1234567 ou 1234-567",
    )
    comment = models.TextField(
        "Commentaire",
        blank=True,
        null=True,
        help_text="Ajoutez un commentaire à propos du bénéficiaire",
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_gender(self):
        return self.GENDERS[self.gender]

    def get_status(self):
        return self.STATUSES[self.status]

    def get_huuid(self):
        return "{}-{}".format(str(self.uuid)[:4], str(self.uuid)[4:7])

    def get_entity_verbose_name(self):
        return self._meta.verbose_name

    def get_anonymized_phone_number(self):
        return "XX XX XX {}".format(self.phone_number.as_national[8:])

    class Meta:
        permissions = (
            ("assign_beneficiary", "assign beneficiary"),
            ("end_of_support_beneficiary", "end of support beneficiary"),
            ("change_beneficiary_phone", "change beneficiary phone"),
            ("view_subsidies", "view subsidies"),
        )
        verbose_name = "bénéficiaire"


class BeneficiaryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        PLACEHOLDER_FORMAT = "Inscrivez votre {field} ici"

        self.fields["first_name"].widget.attrs.update(
            {"placeholder": PLACEHOLDER_FORMAT.format(field="prénom")}
        )
        self.fields["last_name"].widget.attrs.update(
            {"placeholder": PLACEHOLDER_FORMAT.format(field="nom")}
        )
        self.fields["phone_number"].widget.attrs.update(
            {"placeholder": PLACEHOLDER_FORMAT.format(field="numéro de téléphone")}
        )
        self.fields["gender"].widget.attrs.update({"class": "form-check-input"})
        self.fields["structure_counselor"].widget.attrs.update({"readonly": "readonly"})
        self.fields["structure_counselor"].label = "Structure"

    def clean(self):
        try:
            current_beneficiary_id = self.instance.id

            cleaned_data = super().clean()
            first_name = cleaned_data.get("first_name")
            last_name = cleaned_data.get("last_name")
            phone_number = cleaned_data.get("phone_number")

            similar_beneficiary = Beneficiary.objects.get(
                first_name__unaccent__icontains=first_name,
                last_name__unaccent__icontains=last_name,
                phone_number=phone_number,
            )

        except ValueError:
            raise ValidationError("Le formulaire est vide")

        # No similar beneficiary, so pass
        except ObjectDoesNotExist:
            return

        # Similar beneficiary is the current user, so pass
        if similar_beneficiary.id == current_beneficiary_id:
            return

        raise ValidationError(
            mark_safe(
                '<a href="{url}">Utilisateur</a> déjà existant'
                " avec le même nom et numéro de téléphone".format(
                    url=reverse("beneficiaries:show", args=[similar_beneficiary.id])
                )
            )
        )

    class Meta:
        model = Beneficiary
        fields = "__all__"
        exclude = ["status", "structure_operator", "uuid"]
        widgets = {
            "gender": forms.RadioSelect(),
            "status": forms.HiddenInput(),
            "structure_counselor": forms.HiddenInput(),
        }
        error_messages = {
            "phone_number": {"unique": "Ce numéro de téléphone est déjà utilisé"}
        }


class BeneficiaryFormPhone(ModelForm):
    class Meta:
        model = Beneficiary
        fields = ["phone_number"]


class BeneficiaryFormSearch(ModelForm):
    search_by_uuid = forms.CharField(
        validators=[validators.MinLengthValidator(7), validators.MaxLengthValidator(8)],
        help_text="Formats acceptés : xxxxxxx ou xxxx-xxx",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["phone_number"].widget.attrs.update(
            {
                "placeholder": "Rechercher par numéro de téléphone",
                "type": "tel",
                "minlength": 10,
                "size": 35,
            }
        )
        self.fields["phone_number"].required = False

        self.fields["search_by_uuid"].widget.attrs.update(
            {
                "placeholder": "Rechercher par code d’identification",
                "minlength": 7,
                "maxlength": 8,
            }
        )
        self.fields["search_by_uuid"].required = False

    def clean(self):
        cleaned_data = super().clean()
        uuid = cleaned_data.get("search_by_uuid")
        phone_number = cleaned_data.get("phone_number")

        if not uuid and not phone_number:
            raise ValidationError(
                mark_safe(
                    "Pour effectuer la recherche de bénéficiaire,<br/>"
                    "<strong>au moins 1 des 2 champs"
                    "doit être renseigné</strong>."
                )
            )

    class Meta:
        model = Beneficiary
        fields = ["search_by_uuid", "phone_number"]


class BeneficiaryFormAssign(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # We want to display the structure name
        # We need to submit structure id, but :
        # #select doesn't have readonly attribut so user can change, and
        # disabled attribut dont submit the field
        # -> So the structure name is displayed
        #    by the template through user informations

    class Meta:
        model = Beneficiary
        fields = ["status", "structure_operator"]
        widgets = {
            "status": forms.HiddenInput(),
            "structure_operator": forms.HiddenInput(),
        }


class BeneficiaryFormEndOfSupport(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"].widget.attrs.update({"readonly": "readonly"})
        self.fields["last_name"].widget.attrs.update({"readonly": "readonly"})

    class Meta:
        model = Beneficiary
        fields = ["status", "structure_operator", "first_name", "last_name"]
        widgets = {
            "status": forms.HiddenInput(),
            "structure_operator": forms.HiddenInput(),
        }


class Subsidy:
    subsidies_by_year = {
        "2024": {
            "year": 2024,
            "total": 0,
            "new": 0,
            "startSupportCount": 0,
            "endSupportCount": 0,
            "t1": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t2": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t3": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t4": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
        },
        "2025": {
            "year": 2025,
            "total": 0,
            "new": 0,
            "startSupportCount": 0,
            "endSupportCount": 0,
            "t1": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t2": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t3": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t4": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
        },
        "2026": {
            "year": 2026,
            "total": 0,
            "new": 0,
            "startSupportCount": 0,
            "endSupportCount": 0,
            "t1": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t2": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t3": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
            "t4": {
                "subsidies": 0,
                "startSupportCount": 0,
                "endSupportCount": 0,
                "new": 0,
            },
        },
    }
    subsidies_by_city = {
        "fullname": None,
        "per_year": {
            "2024": {"total": 0, "startSupportCount": 0, "endSupportCount": 0},
            "2025": {"total": 0, "startSupportCount": 0, "endSupportCount": 0},
            "2026": {"total": 0, "startSupportCount": 0, "endSupportCount": 0},
        },
    }
    subsidies_by_structure = {
        "per_year": {
            "2024": {"total": 0, "startSupportCount": 0, "endSupportCount": 0},
            "2025": {"total": 0, "startSupportCount": 0, "endSupportCount": 0},
            "2026": {"total": 0, "startSupportCount": 0, "endSupportCount": 0},
        }
    }
