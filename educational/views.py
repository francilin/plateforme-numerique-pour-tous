# educational/views.py
from django.shortcuts import render

def educational_index(request):
    return render(request, "educational/educational_index.html", {"url_name": "educational_index"})
