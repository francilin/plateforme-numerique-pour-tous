from django.http import HttpResponseRedirect
from django.urls import reverse
from django.conf import settings


class AuthRequiredMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        redirect_url = reverse("login")

        if (
            request.path.startswith('/compte')
            or request.path.startswith(settings.STATIC_URL)
        ):
            return self.get_response(request)

        if not request.user.is_authenticated:
            return HttpResponseRedirect(
                redirect_url + '?next=' + request.path
            )

        return self.get_response(request)
