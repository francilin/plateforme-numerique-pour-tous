import uuid

from django.contrib.auth.models import Group

from beneficiaries.models import Beneficiary
from cities.models import City
from structures.models import Structure
from support_logs.models import SupportLogs
from users.models import (
    GROUP_CITY_NAME,
    GROUP_COUNSELOR_NAME,
    GROUP_OPERATOR_NAME,
    User,
)


def create_beneficiary(
    gender="M",
    first_name="John",
    last_name="Doe",
    age="16-19",
    phone_number="0600000000",
    status=Beneficiary.CREATED,
    structure_counselor=None,
    structure_operator=None,
    comment=None,
):
    uuid = _generate_uuid()
    return Beneficiary.objects.create(
        gender=gender,
        first_name=first_name,
        last_name=last_name,
        age=age,
        phone_number=phone_number,
        uuid=uuid,
        status=status,
        structure_counselor=structure_counselor,
        structure_operator=structure_operator,
        comment=comment,
    )


def create_operator(
    email="operator@ma.il", password="verysecret", structure_name="Structure opérateur"
):
    return _create_user_with_group_and_structure(
        GROUP_OPERATOR_NAME, structure_name, email=email, password=password
    )


def create_operator_counselor(
    email="operator_counselor@ma.il",
    password="verysecret",
    structure_name="Structure opérateur-orienteur",
):
    return _create_user_with_groups_and_structure(
        [GROUP_OPERATOR_NAME, GROUP_COUNSELOR_NAME],
        structure_name,
        email=email,
        password=password,
    )


def create_admin():
    return User.objects.create_superuser(email="super@user.com", password="supersecret")


def create_counselor(email="counselor@ma.il", password="verysecret"):
    return _create_user_with_group_and_structure(
        GROUP_COUNSELOR_NAME, "Structure orienteur", email=email, password=password
    )


def create_city_user(email="commune@ma.il", password="verysecret", city=None):
    if city is None:
        city = City.objects.last()

    return _create_user_with_group_and_structure(
        GROUP_CITY_NAME, "Compte commune", email=email, password=password, city=city
    )


def _create_user_with_group_and_structure(
    group_name,
    structure_name,
    city=None,
    email="e@ma.il",
    password="verysecret",
):
    return _create_user_with_groups_and_structure(
        [group_name],
        structure_name,
        city,
        email,
        password,
    )


def _create_user_with_groups_and_structure(
    group_names,
    structure_name,
    city=None,
    email="e@ma.il",
    password="verysecret",
):
    user = User.objects.create_user(email, password)

    for group_name in group_names:
        group = Group.objects.get(name=group_name)
        user.groups.add(group)

    if not Structure.objects.filter(name=structure_name).exists():
        structure = Structure(name=structure_name)
        structure.save()
    else:
        structure = Structure.objects.get(name=structure_name)

    user.structure = structure

    user.city = city

    user.save()

    return user


def _generate_uuid():
    return uuid.uuid4()


def _support_log_helper(status, operator, actionDate, city=None, beneficiary=None):
    args = {}

    if beneficiary:
        args["beneficiary"] = beneficiary

    if status == Beneficiary.ACTIVE:
        if city:
            args["city"] = city

    return SupportLogs(
        status=status,
        operator=operator,
        actionDate=actionDate,
        **args,
    )


def make_start_of_support_log(operator, city, beneficiary=None, actionDate=None):
    return _support_log_helper(
        Beneficiary.ACTIVE, operator, actionDate, city, beneficiary
    )


def make_end_of_support_log(operator, beneficiary=None, actionDate=None):
    return _support_log_helper(
        Beneficiary.DONE,
        operator,
        actionDate,
        None,
        beneficiary,
    )
