from django.apps import AppConfig


class SupportThemesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'support_themes'
    verbose_name = "Thèmes de support"
