from django.db import models


class SupportTheme(models.Model):
    title = models.CharField(
        "Titre",
        max_length=200,
        null=False,
        blank=False,
    )
    position = models.IntegerField(
        "Position",
        null=False,
        blank=False
    )
    description = models.TextField(
        "Description",
        blank=True,
        null=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Theme de support"
        verbose_name_plural = "Themes de support"
