from django.contrib import admin

from .models import SupportTheme


@admin.register(SupportTheme)
class SupportThemeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "position",
        "description"
    )
