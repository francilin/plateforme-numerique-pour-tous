from django.db import models
import django.db.models.deletion
from django.contrib.auth.models import AbstractUser, BaseUserManager

from structures.models import Structure
from cities.models import City

GROUP_COUNSELOR_NAME = "Orienteur"
GROUP_OPERATOR_NAME = "Opérateur"
GROUP_CITY_NAME = "Commune"


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The given email must be set")

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)

        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True")

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True")

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True)
    structure = models.ForeignKey(
        Structure,
        on_delete=django.db.models.deletion.SET_NULL,
        blank=True,
        null=True,
        help_text="Requis pour les groupes 'Orienteur' & 'Opérateur'"
    )
    city = models.ForeignKey(
        City,
        on_delete=django.db.models.deletion.SET_NULL,
        blank=True,
        null=True,
        help_text="Requis pour le groupe 'Commune'",
        verbose_name="Commune"
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def is_operator(self):
        return self.groups.filter(name=GROUP_OPERATOR_NAME).exists()

    def is_counselor(self):
        return self.groups.filter(name=GROUP_COUNSELOR_NAME).exists()

    def is_city_group(self):
        return self.groups.filter(name=GROUP_CITY_NAME).exists()
