from django.contrib.auth.models import Group
from django.test import TestCase
from django.urls import reverse

from cities.models import City
from structures.models import Structure
from users.models import (
    GROUP_CITY_NAME,
    GROUP_COUNSELOR_NAME,
    GROUP_OPERATOR_NAME,
    User
)


class CreateTest(TestCase):
    def test_create_user_counselor(self):
        email = "test@user.com"
        password = "verysecret"
        structure = Structure(name="Structure de test")
        structure.save()

        super_user = User.objects.create_superuser("super@user.mail", password)

        self.client.force_login(super_user)
        response = self.client.post(
            reverse("admin:users_user_add"),
            data={
                "structure": structure.id,
                "groups": Group.objects.get(name=GROUP_COUNSELOR_NAME).id,
                "email": email,
                "password1": password,
                "password2": password,
            },
            follow=True
        )

        self.assertContains(
            response,
            '<a href="{}">{}</a>&nbsp;'.format(
                reverse(
                    "admin:users_user_change",
                    args=[User.objects.last().id]
                ),
                email
            ),
            html=True
        )

    def test_create_user_operator(self):
        email = "test@user.com"
        password = "verysecret"
        structure = Structure(name="Structure de test")
        structure.save()

        super_user = User.objects.create_superuser("super@user.mail", password)

        self.client.force_login(super_user)
        response = self.client.post(
            reverse("admin:users_user_add"),
            data={
                "structure": structure.id,
                "groups": Group.objects.get(name=GROUP_OPERATOR_NAME).id,
                "email": email,
                "password1": password,
                "password2": password,
            },
            follow=True
        )

        self.assertContains(
            response,
            '<a href="{}">{}</a>&nbsp;'.format(
                reverse(
                    "admin:users_user_change",
                    args=[User.objects.last().id]
                ),
                email
            ),
            html=True
        )

    def test_create_user_city(self):
        email = "test@user.com"
        password = "verysecret"

        super_user = User.objects.create_superuser("super@user.mail", password)

        self.client.force_login(super_user)
        response = self.client.post(
            reverse("admin:users_user_add"),
            data={
                "city": City.objects.first().id,
                "groups": Group.objects.get(name=GROUP_CITY_NAME).id,
                "email": email,
                "password1": password,
                "password2": password,
            },
            follow=True
        )

        self.assertContains(
            response,
            '<a href="{}">{}</a>&nbsp;'.format(
                reverse(
                    "admin:users_user_change",
                    args=[User.objects.last().id]
                ),
                email
            ),
            html=True
        )

    def test_create_user_with_bad_password_confirmation_fail(self):
        email = "test@user.com"
        password = "verysecret"
        structure = Structure(name="Structure de test")
        structure.save()

        super_user = User.objects.create_superuser("super@user.mail", password)

        self.client.force_login(super_user)
        response = self.client.post(
            reverse("admin:users_user_add"),
            data={
                "structure": structure.id,
                "groups": Group.objects.last().id,
                "email": email,
                "password1": password,
                "password2": "another pass",
            },
            follow=True
        )

        self.assertContains(response, "Les mots de passe différent")
