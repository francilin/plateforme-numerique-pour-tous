from django.test import TestCase
from django.urls import reverse

from users.models import User
from core.tests.utils import create_counselor


class LoginTest(TestCase):
    def test_login_success(self):
        email = "test@user.com"
        password = "verysecret"
        User.objects.create_user(email, password)

        response = self.client.post(
            reverse("login"),
            data={"username": email, "password": password},
            follow=True
        )

        self.assertRedirects(response, reverse("beneficiaries:index"))

    def test_login_success_redirect_to_next_url(self):
        email = "test@user.com"
        password = "verysecret"
        create_counselor(email=email, password=password)

        response = self.client.post(
            reverse(
                "login"
            ) + "?next=" + reverse(
                "beneficiaries:new"
            ),
            data={"username": email, "password": password},
            follow=True
        )

        self.assertRedirects(
            response,
            reverse("beneficiaries:new")
        )

    def test_login_fail(self):
        response = self.client.post(
            reverse("login"),
            data={"username": "Jane", "password": "Does"},
            follow=True
        )

        self.assertContains(
            response,
            "Saisissez un email et un mot de passe valides."
        )

    def test_redirect_unauthenticated_user(self):
        response = self.client.get(
            reverse("beneficiaries:index"),
            follow=True
        )

        self.assertRedirects(
            response,
            reverse("login") + "?next=" + reverse("beneficiaries:index")
        )
        self.assertContains(
            response,
            "Connexion"
        )

    def test_logout(self):
        user = User.objects.create()
        self.client.force_login(user)

        response = self.client.post(
            reverse("logout"),
            follow=True
        )

        self.assertRedirects(response, reverse("login"))
        self.assertContains(
            response,
            "Connexion"
        )
