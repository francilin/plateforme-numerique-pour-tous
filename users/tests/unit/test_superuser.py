from django.test import TestCase

from users.models import User


class SuperUserTest(TestCase):
    def test_create_superuser_success(self):
        email = "super@e.mail"
        password = "bépo"
        super_user = User.objects.create_superuser(email, password)

        self.assertEqual(super_user.email, email)
        self.assertNotEqual(super_user.password, password)
        self.assertTrue(super_user.is_staff)
        self.assertTrue(super_user.is_superuser)
