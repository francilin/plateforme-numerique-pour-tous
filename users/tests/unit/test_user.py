from django.test import TestCase

from users.models import User


class UserTest(TestCase):
    def test_create_user_success(self):
        email = "user@e.mail"
        password = "bépo"
        super_user = User.objects.create_user(email, password)

        self.assertEqual(super_user.email, email)
        self.assertNotEqual(super_user.password, password)
        self.assertFalse(super_user.is_staff)
        self.assertFalse(super_user.is_superuser)
