from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db.models import Q

from .models import (
    GROUP_COUNSELOR_NAME,
    GROUP_OPERATOR_NAME,
    GROUP_CITY_NAME,
    User
)


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(
        label="Mot de passe",
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label="Confirmez votre mot de passe",
        widget=forms.PasswordInput
    )
    groups = forms.ModelMultipleChoiceField(
        queryset=Group.objects.all()
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["groups"].label = "Groupe"

    class Meta:
        model = User
        fields = ["email", "structure", "city"]

    def clean(self):
        if (self.cleaned_data.get("groups") is None):
            return

        if (
            self.cleaned_data.get("groups").filter(
                name=GROUP_CITY_NAME
            ).exists()
            and self.cleaned_data.get("city") is None
        ):
            raise ValidationError(
                "Le champ 'Commune' est requis pour le groupe 'Commune'"
            )

        if (
            self.cleaned_data.get("groups").filter(
                Q(name=GROUP_COUNSELOR_NAME) | Q(name=GROUP_OPERATOR_NAME)
            ).exists()
            and self.cleaned_data.get("structure") is None
        ):
            raise ValidationError(
                "Le champ 'Structure' est requis pour"
                " les groupes 'Orienteur' & 'Opérateur'"
            )

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise ValidationError("Les mots de passe différent")

        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])

        if commit:
            user.save()

        return user


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    @admin.display(description="Groupe(s)")
    def related_groups(self, obj):
        return ", ".join([
            "{} ".format(
                groups,
            ) for groups in obj.groups.all()
        ])

    # Change form fields
    fieldsets = (
        ("Identifiants", {"fields": ("email", "password")}),
        ("Structure", {"fields": ("structure", )}),
        ("Commune", {"fields": ("city", )}),
        ("Permissions", {
            "fields": (
                "groups",
                "is_active",
                "is_staff",
                "is_superuser",
                "user_permissions"
            )
        }),
        ("Personal info", {"fields": ("first_name", "last_name")}),
        ("Important dates", {"fields": ("last_login", "date_joined")}),
    )

    # Create form fields
    add_fieldsets = (
        (None, {
            "classes": ("wide",),
            "fields": (
                "structure",
                "city",
                "groups",
                "email",
                "password1",
                "password2"
            )
        }),
    )

    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "related_groups"
    )
    search_fields = ("email", "first_name", "last_name")
    ordering = ("-id",)

    add_form = UserCreationForm
