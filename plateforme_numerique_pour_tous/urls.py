from django.conf import settings

"""
URL configuration for plateforme_numerique_pour_tous project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.urls import include, path

from beneficiaries import views as beneficiaries_views
from directory import views as directory_views
from educational import views as educational_views

urlpatterns = [
    path("", lambda _: redirect("/beneficiaires")),
    path("beneficiaires/", include("beneficiaries.urls")),
    path("admin/", admin.site.urls),
    path("compte/", include("django.contrib.auth.urls")),
    path("compte/connexion/", auth_views.LoginView.as_view(), name="login"),
    path(
        "compte/mot-de-passe-oublie/",
        auth_views.PasswordResetView.as_view(),
        name="password_reset",
    ),
    path(
        "compte/mot-de-passe-oublie/envoye",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "subventions",
        beneficiaries_views.subsidies_index,
        name="subsidies_index"
    ),
    path(
        "repertoire", 
        directory_views.structures_index, 
        name="structures_index"
    ),
    path(
        "contenus-pedagogiques", 
        educational_views.educational_index, 
        name="educational_index"
    ),
]

if settings.DEBUG:
    urlpatterns += path(
        "__debug__/",
        include("debug_toolbar.urls")
    ),
